#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do
    echo "./script_EDDYQC_subject.py $elem"
done > jobs_EDDYQC.txt

jid=`fsl_sub -q long.qc -l logs -t jobs_EDDYQC.txt`

fsl_sub -j $jid -q long.qc -l logs ./script_EDDYQC_gather.sh

