#!/bin/env python

import os
import sys
import argparse
import numpy as np

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():
    parser = MyParser(description='BioBank Pipeline Manager')
    parser.add_argument("subjectFolder", help='Subject Folder')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    subject = os.environ['subjDir'] + '/' + subject

    if os.path.exists(subject + '/fMRI/unusable/rfMRI.ica/mc/prefiltered_func_data_mcf_abs.rms'):
        originalFile_rfMRI_abs = subject + '/fMRI/unusable/rfMRI.ica/mc/prefiltered_func_data_mcf_abs.rms'
    else:
        originalFile_rfMRI_abs = subject + '/fMRI/rfMRI.ica/mc/prefiltered_func_data_mcf_abs.rms'

    if os.path.exists(subject + '/fMRI/unusable/rfMRI.ica/mc/prefiltered_func_data_mcf_rel.rms'):
        originalFile_rfMRI_rel = subject + '/fMRI/unusable/rfMRI.ica/mc/prefiltered_func_data_mcf_rel.rms'
    else:
        originalFile_rfMRI_rel = subject + '/fMRI/rfMRI.ica/mc/prefiltered_func_data_mcf_rel.rms'


    if os.path.exists(subject + '/fMRI/unusable/tfMRI.feat/mc/prefiltered_func_data_mcf_abs.rms'):
        originalFile_tfMRI_abs = subject + '/fMRI/unusable/tfMRI.feat/mc/prefiltered_func_data_mcf_abs.rms'
    else:
        originalFile_tfMRI_abs = subject + '/fMRI/tfMRI.feat/mc/prefiltered_func_data_mcf_abs.rms'

    if os.path.exists(subject + '/fMRI/unusable/tfMRI.feat/mc/prefiltered_func_data_mcf_rel.rms'):
        originalFile_tfMRI_rel = subject + '/fMRI/unusable/tfMRI.feat/mc/prefiltered_func_data_mcf_rel.rms'
    else:
        originalFile_tfMRI_rel = subject + '/fMRI/tfMRI.feat/mc/prefiltered_func_data_mcf_rel.rms'


    if os.path.exists(subject + '/dMRI/unusable/dMRI/data.eddy_movement_rms'):
        originalFile_dMRI_motion = subject + '/dMRI/unusable/dMRI/data.eddy_movement_rms'
    elif os.path.exists(subject + '/dMRI/incompatible/dMRI/data.eddy_movement_rms'):
        originalFile_dMRI_motion = subject + '/dMRI/incompatible/dMRI/data.eddy_movement_rms'
    elif os.path.exists(subject + '/dMRI/unusable/incompatible/dMRI/data.eddy_movement_rms'):
        originalFile_dMRI_motion = subject + '/dMRI/unusable/incompatible/dMRI/data.eddy_movement_rms'
    else:
        originalFile_dMRI_motion = subject + '/dMRI/dMRI/data.eddy_movement_rms'

    if os.path.exists(subject + '/dMRI/unusable/dMRI/data.eddy_outlier_report'):
        originalFile_dMRI_outliers = subject + '/dMRI/unusable/dMRI/data.eddy_outlier_report'
    elif os.path.exists(subject + '/dMRI/incompatible/dMRI/data.eddy_outlier_report'):
        originalFile_dMRI_outliers = subject + '/dMRI/incompatible/dMRI/data.eddy_outlier_report'
    elif os.path.exists(subject + '/dMRI/unusable/incompatible/dMRI/data.eddy_outlier_report'):
        originalFile_dMRI_outliers = subject + '/dMRI/unusable/incompatible/dMRI/data.eddy_outlier_report'
    else:
        originalFile_dMRI_outliers = subject + '/dMRI/dMRI/data.eddy_outlier_report'



    result=subject.split('/')[-1] + ' '
    
    if os.path.exists(originalFile_rfMRI_abs):
        raw_values = np.loadtxt(originalFile_rfMRI_abs)
        result = result + str(np.nanmean(raw_values)) + ' ' +\
                           str(np.nanpercentile(raw_values,50)) + ' ' +\
                           str(np.nanpercentile(raw_values,90)) + ' ' 
    else:
        result = result + 'NaN NaN NaN '

    if os.path.exists(originalFile_rfMRI_rel):
        raw_values = np.loadtxt(originalFile_rfMRI_rel)
        result = result + str(np.nanmean(raw_values)) + ' ' +\
                           str(np.nanpercentile(raw_values,50)) + ' ' +\
                           str(np.nanpercentile(raw_values,90)) + ' ' 
    else:
        result = result + 'NaN NaN NaN '


    if os.path.exists(originalFile_tfMRI_abs):
        raw_values = np.loadtxt(originalFile_tfMRI_abs)
        result = result + str(np.nanmean(raw_values)) + ' ' +\
                           str(np.nanpercentile(raw_values,50)) + ' ' +\
                           str(np.nanpercentile(raw_values,90)) + ' ' 
    else:
        result = result + 'NaN NaN NaN '

    if os.path.exists(originalFile_tfMRI_rel):
        raw_values = np.loadtxt(originalFile_tfMRI_rel)
        result = result + str(np.nanmean(raw_values)) + ' ' +\
                           str(np.nanpercentile(raw_values,50)) + ' ' +\
                           str(np.nanpercentile(raw_values,90)) + ' ' 
    else:
        result = result + 'NaN NaN NaN '


    if os.path.exists(originalFile_dMRI_motion):
        raw_values = np.loadtxt(originalFile_dMRI_motion)
        result = result + str(np.nanmean(raw_values[:,0])) + ' ' +\
                          str(np.nanpercentile(raw_values[:,0],50)) + ' ' +\
                          str(np.nanpercentile(raw_values[:,0],90)) + ' ' +\
                          str(np.nanmean(raw_values[:,1])) + ' ' +\
                          str(np.nanpercentile(raw_values[:,1],50)) + ' ' +\
                          str(np.nanpercentile(raw_values[:,1],90)) + ' ' 
    else:
        result = result + 'NaN NaN NaN NaN NaN NaN '
    
    if os.path.exists(originalFile_dMRI_outliers):
        result = result + str(sum(1 for line in open(originalFile_dMRI_outliers)))
    else:
        result = result + 'NaN'

    print(result)


if __name__ == "__main__":
    main()
