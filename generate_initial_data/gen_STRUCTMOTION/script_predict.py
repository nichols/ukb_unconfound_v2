#!/bin/env python

import os
import pickle
import numpy as np 
from file_to_dict import file_to_dict


# Generate dictionaries with the data
test_data_dict    = file_to_dict('PREDICT_DATA/predict_data.txt')

# Get the lists of subjects 
test_subjects = list(test_data_dict.keys())
test_subjects.sort()

num_test_subjects = len(test_subjects)
num_vars = len(test_data_dict[test_subjects[0]])

# Create a matrix with the features of the test set
test_data = np.zeros((num_test_subjects, num_vars))
for i in range(num_test_subjects):
    test_data[i,:] = test_data_dict[test_subjects[i]]  

# Read the mean and std of the total population
means_total = np.loadtxt('NORM/means.txt')
stds_total  = np.loadtxt('NORM/stds.txt')
    
# Normalise the test set and remove NaNs
test_data_norm = np.zeros(test_data.shape)
for i in range(num_vars):
    test_data[np.isnan(test_data[:,i]),i] = means_total[i] 
    test_data_norm[:,i] = (test_data[:,i] - means_total[i]) / stds_total[i] 

# Prediction for the data
final_model = pickle.load(open('MODEL/model.p','rb'))
prediction  = final_model.predict(test_data_norm)

# Saving result
with open(os.environ['CONFDATA'] + '/ID_STRUCTHEADMOTION.txt', 'w') as f:
    for i in range(num_test_subjects):
        f.write(test_subjects[i] + " " + str(prediction[i])+'\n')
f.close()
