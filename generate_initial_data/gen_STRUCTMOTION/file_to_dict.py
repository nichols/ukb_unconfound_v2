#!/bin/env python

# Function to convert a file into a dictionary (1st column is the dictionary key)
def file_to_dict(fileName):
    import numpy as np 

    dicti={}
    with open(fileName, 'rb') as f:
        num_cols = len(f.readline().split())
        f.seek(0)
        data = np.genfromtxt(f, usecols=range(1,num_cols))
        data = np.reshape(data, (data.shape[0], num_cols-1))
        f.seek(0)
        subjs = []
        for line in f:
            subjs.append(line.split()[0].decode())     
    for i in range(len(subjs)):
        dicti[subjs[i]] = data[i,:]
    return dicti

