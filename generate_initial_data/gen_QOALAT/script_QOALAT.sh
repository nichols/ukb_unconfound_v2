#!/bin/bash

path="$SUBJECTS_DIR"
sleep 1
echo "This bash script will create table from ?.stats files"
echo "Written by Jamaan Alghamdi & Dr. Vanessa Sluming"
echo "University of Liverpool"
echo "jamaan.alghamdi@gmail.com"
echo "http://www.easyneuroimaging.com"
echo "20/12/2010"
echo ""
echo "Adapted for UKBB by Fidel Alfaro Almagro"
echo "01/08/2019"

deactivate
# asegstats2table needs python 2.7
source $BB_BIN_DIR/bb_python/bb_python-2.7.12/bin/activate

rm   -rf TEST_DATA logs
mkdir -p TEST_DATA logs
list=`cat $CONFDATA/FS_subj.txt`
asegstats2table  --subjects $list --all-segs --meas volume    --skip --tablefile TEST_DATA/aseg_stats.txt         &> logs/aseg_log.txt 
aparcstats2table --subjects $list --hemi lh  --meas volume    --skip --tablefile TEST_DATA/aparc_volume_lh.txt    &> logs/lh_volume_log.txt 
aparcstats2table --subjects $list --hemi lh  --meas thickness --skip --tablefile TEST_DATA/aparc_thickness_lh.txt &> logs/lh_thickness_log.txt 
aparcstats2table --subjects $list --hemi lh  --meas area      --skip --tablefile TEST_DATA/aparc_area_lh.txt      &> logs/lh_area_log.txt 
aparcstats2table --subjects $list --hemi rh  --meas volume    --skip --tablefile TEST_DATA/aparc_volume_rh.txt    &> logs/rh_volume_log.txt 
aparcstats2table --subjects $list --hemi rh  --meas thickness --skip --tablefile TEST_DATA/aparc_thickness_rh.txt &> logs/rh_thickness_log.txt 
aparcstats2table --subjects $list --hemi rh  --meas area      --skip --tablefile TEST_DATA/aparc_area_rh.txt      &> logs/rh_area_log.txt 

source $BB_BIN_DIR/bb_python/bb_python/bin/activate

dire=./TEST_DATA/
orig_dire=`pwd`

cd $dire
for elem in `cat ../files.txt` ; do
  num_vars=`cat ../headers/headers_$elem | head -n 1 | wc -w | awk '{print $1}'`
  cat $elem | awk -v val=$num_vars '{ for (i=1;i<=val;i++){ printf $i" " }; printf "\n"}' |tail -n +2 > tmp
  cat ../headers/headers_$elem | awk -v val=$num_vars '{ for (i=1;i<=val;i++){ printf $i" " }; printf "\n"}'| awk '$1=$1' > $elem
  cat tmp  | column -t | sed 's|\t| |g' | awk '$1=$1' >> $elem
done

echo "Visit_ID Rating" > tmp
cat `cat ../files.txt | head -n 1` |tail -n +2| awk '{print $1,"NA"}' > tmp2
cat tmp tmp2 | column -t | awk '$1=$1'> class.txt

rm tmp tmp2

cd $orig_dire


###########################
# Run Qoala-T on the data #
###########################
~/R/bin/Rscript "QoalaT.R" &> logs/QoalaT_log.txt

#Save Results
cat Qoala_T_predictions_UKB.csv | awk -F ',' '{print $1,$2}' | sed 's|"||g' | grep -v Visit| sort> tmp.txt

for sub in `cat $CONFDATA/subj.txt` ; do
    t=`cat tmp.txt | grep "^$sub"`
    if [ "$t" == "" ] ; then
        echo "$sub NaN"
    else
        echo $t
    fi
done > $CONFDATA/ID_QOALAT.txt

rm tmp.txt
