#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/dMRI/dMRI/data.eddy_command_txt ] ; then 
        echo $elem `cat $subjDir/$elem/dMRI/dMRI/data.eddy_command_txt | awk '{print $1}'` ; 
    else 
        echo $elem NaN ; 
    fi ; 
done | sed 's|NaN|0|g' | sed 's|/well/win/projects/ukbiobank/fbp/bb_FSL/bin/eddy_cuda8.0_test|1|g' > $CONFDATA/ID_NEWEDDY.txt


