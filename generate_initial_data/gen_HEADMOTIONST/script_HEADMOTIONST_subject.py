#!/bin/env python

import os
import sys
import argparse
import numpy as np
import nibabel as nib
import numpy.linalg as npl
from nibabel.affines import apply_affine

def get_best_affine(im, preference='sform'):
    if preference == 'sform' and (im.header['sform_code'] > 0):
        return im.get_sform()
    if preference == 'qform' and (im.header['qform_code'] > 0):
        return im.get_qform()
    return im.affine

def vox2fslmm_mat(im):
    [dx, dy, dz] = im.header.get_zooms()[:3]
    vox2mm_mat = np.matrix(np.zeros([4, 4]))
    vox2mm_mat[0, 0] = dx
    vox2mm_mat[1, 1] = dy
    vox2mm_mat[2, 2] = dz
    vox2mm_mat[3, 3] = 1

    if np.linalg.det(get_best_affine(im)) > 0:
        vox2mm_mat[0, 0] = -dx
        nx = im.get_data().shape[0]
        vox2mm_mat[0, 3] = (nx - 1) * dx
    return vox2mm_mat


def ref2positionN(points_in_ref, n, mcMatrix, exampleFunc):
    ref_aff=vox2fslmm_mat(exampleFunc)
    ref_aff_inv = npl.inv(ref_aff)
    m=np.matrix(mcMatrix[(n*4):((n*4)+4),:])
    m_inv=npl.inv(m)

    return ref_aff_inv * m_inv * ref_aff * points_in_ref


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():
    parser = MyParser(description='BioBank Pipeline Manager')
    parser.add_argument("subjectFolder", help='Subject Folder')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    subject = os.environ['subjDir'] + '/' + subject

    if os.path.exists(subject + '/fMRI/unusable/rfMRI.nii.gz'):
        originalFile = subject + '/fMRI/unusable/rfMRI.nii.gz'
        mcDir = subject + '/fMRI/unusable/rfMRI.ica/mc/prefiltered_func_data_mcf.mat/'
        mcFile = subject + '/fMRI/unusable/rfMRI.ica/mc/prefiltered_func_data_mcf.cat'
        exampleFuncFile = subject + '/fMRI/unusable/rfMRI.ica/example_func.nii.gz'
        maskFile = subject + '/fMRI/unusable/rfMRI.ica/mask.nii.gz'
    else:
        originalFile = subject + '/fMRI/rfMRI.nii.gz'
        mcDir = subject + '/fMRI/rfMRI.ica/mc/prefiltered_func_data_mcf.mat/'
        mcFile = subject + '/fMRI/rfMRI.ica/mc/prefiltered_func_data_mcf.cat'
        exampleFuncFile = subject + '/fMRI/rfMRI.ica/example_func.nii.gz'
        maskFile = subject + '/fMRI/rfMRI.ica/mask.nii.gz'

    for fileName in (originalFile, exampleFuncFile, maskFile):
        if not os.path.exists(fileName):
            print(subject.split('/')[-1] + ' NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN')
            sys.exit(0)

    if not os.path.exists(mcDir):
        if not os.path.exists(mcFile):
            print(subject.split('/')[-1] + ' NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN')
            sys.exit(0)


    original = nib.load(originalFile)
    exampleFunc = nib.load(exampleFuncFile)
    mask = nib.load(maskFile)

    [indX, indY, indZ] = np.where(mask.dataobj[:,:,:] == 1.0)

    num_vecs = indX.size

    points = [indX, indY, indZ, np.ones(num_vecs)]

    num_timepoints = original.header['dim'][4] -1

    if os.path.exists(mcFile):
        mcMatrix = np.matrix(np.loadtxt(mcFile))
    else:
        mcMatrix = np.matrix(np.loadtxt(mcDir + 'MAT_0000'))
        for i in range(1,num_timepoints+1):
            mcMatrix = np. concatenate((mcMatrix,np.matrix(np.loadtxt(mcDir + 'MAT_' + ( "%04d" % i) ))))

    result=[]

    for i in range(num_timepoints):

        new_points_0 = ref2positionN(points, i,   mcMatrix, exampleFunc)
        new_points_1 = ref2positionN(points, i+1, mcMatrix, exampleFunc)
        
        result.append([npl.norm([new_points_1[0]-new_points_0[0], \
                                 new_points_1[1]-new_points_0[1]],axis=0),\
                             abs(new_points_1[2]-new_points_0[2])])

    result=np.array(result).reshape(num_timepoints,2,num_vecs)
    
    inplane_through_time=np.zeros(num_timepoints)
    Z_through_time=np.zeros(num_timepoints)

    for i in range(num_timepoints):
        inplane_through_time[i]=np.nanmean(result[i,0,:])
        Z_through_time[i]=np.nanmean(result[i,1,:])


    inplane_through_space=np.zeros(num_vecs)
    Z_through_space=np.zeros(num_vecs)

    for i in range(num_vecs):
        inplane_through_space[i]=np.nanmean(result[:,0,i])
        Z_through_space[i]=np.nanmean(result[:,1,i])

    print(subject.split('/')[-1] + ' '+\
          str(np.nanmean(inplane_through_time)) + ' ' +\
          str(np.nanpercentile(inplane_through_time,50)) + ' ' +\
          str(np.nanpercentile(inplane_through_time,90)) + ' ' +\
          str(np.nanmean(Z_through_time)) + ' ' +\
          str(np.nanpercentile(Z_through_time,50)) + ' ' +\
          str(np.nanpercentile(Z_through_time,90)) + ' ' +\
          #str(np.nanmean(inplane_through_space)) + ' ' +\
          str(np.nanpercentile(inplane_through_space,50)) + ' ' +\
          str(np.nanpercentile(inplane_through_space,90)) + ' ' +\
          #str(np.nanmean(Z_through_space)) + ' ' +\
          str(np.nanpercentile(Z_through_space,50)) + ' ' +\
          str(np.nanpercentile(Z_through_space,90)))

    #np.save(subject.split('/')[-1]+'.txt', result)

if __name__ == "__main__":
    main()
