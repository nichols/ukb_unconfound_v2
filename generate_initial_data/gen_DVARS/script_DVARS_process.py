#!/bin/env python

import os
import sys
import argparse
import numpy as np
import shutil

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():

    sDir = os.environ['CONFDATA']
   
    #print("ID " +
    #      "S_dirty_mean S_dirty_median S_dirty_p90 " +
    #      "D_dirty_mean D_dirty_median D_dirty_p90 " + 
    #      "SD_dirty_mean SD_dirty_median SD_dirty_p90 " +
    #      "S_clean_mean S_clean_median S_clean_p90 " +
    #      "D_clean_mean D_clean_median D_clean_p90 " +
    #      "SD_clean_mean SD_clean_median SD_clean_p90")

    textFile = open(sDir+'/subj.txt','r')
    subjects= [x.replace('\n','',) for x in textFile.readlines()]

    for subject in subjects:
        result = '' + subject

        S_dirty_filename = 'results/S_' + subject +  '_dirty.txt'
        D_dirty_filename = 'results/D_' + subject +  '_dirty.txt'
        S_clean_filename = 'results/S_' + subject +  '_clean.txt'
        D_clean_filename = 'results/D_' + subject +  '_clean.txt'

        if (os.path.exists(S_dirty_filename) and 
            os.path.exists(D_dirty_filename) ):

            S_dirty  = np.loadtxt(S_dirty_filename)
            D_dirty  = np.loadtxt(D_dirty_filename)
            SD_dirty = abs(S_dirty - D_dirty)

            S_dirty_mean    = np.nanmean(S_dirty)
            S_dirty_median  = np.nanpercentile(S_dirty,50)
            S_dirty_p90     = np.nanpercentile(S_dirty,90)

            D_dirty_mean    = np.nanmean(D_dirty)
            D_dirty_median  = np.nanpercentile(D_dirty,50)
            D_dirty_p90     = np.nanpercentile(D_dirty,90)

            SD_dirty_mean   = np.nanmean(SD_dirty)
            SD_dirty_median = np.nanpercentile(SD_dirty,50)
            SD_dirty_p90    = np.nanpercentile(SD_dirty,90)

            result = result + ' ' + str(S_dirty_mean) \
            + ' ' + str(S_dirty_median) + ' ' + str(S_dirty_p90) \
            + ' ' + str(D_dirty_mean) + ' ' + str(D_dirty_median) \
            + ' ' + str(D_dirty_p90) + ' ' + str(SD_dirty_mean) \
            + ' ' + str(SD_dirty_median) + ' ' + str(SD_dirty_p90)

        else:
            result = result + ' NaN NaN NaN NaN NaN NaN NaN NaN NaN'
                    
        if (os.path.exists(S_clean_filename) and 
            os.path.exists(D_clean_filename)):

            S_clean  = np.loadtxt(S_clean_filename)
            D_clean  = np.loadtxt(D_clean_filename)
            SD_clean = abs(S_clean - D_clean)

            S_clean_mean    = np.nanmean(S_clean)
            S_clean_median  = np.nanpercentile(S_clean,50)
            S_clean_p90     = np.nanpercentile(S_clean,90)

            D_clean_mean    = np.nanmean(D_clean)
            D_clean_median  = np.nanpercentile(D_clean,50)
            D_clean_p90     = np.nanpercentile(D_clean,90)

            SD_clean_mean   = np.nanmean(SD_clean)
            SD_clean_median = np.nanpercentile(SD_clean,50)
            SD_clean_p90    = np.nanpercentile(SD_clean,90)

            result = result + ' ' + str(S_clean_mean) \
            + ' ' + str(S_clean_median) + ' ' + str(S_clean_p90) \
            + ' ' + str(D_clean_mean) + ' ' + str(D_clean_median) \
            + ' ' + str(D_clean_p90) + ' ' + str(SD_clean_mean) \
            + ' ' + str(SD_clean_median) + ' ' + str(SD_clean_p90)
        else:
            result = result + ' NaN NaN NaN NaN NaN NaN NaN NaN NaN'
                    
        print(result)


if __name__ == "__main__":
    main()
