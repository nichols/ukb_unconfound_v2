#!/bin/bash

num_IDPs="892";
num_d25_Ampl="21";
num_d25_Full="210";
num_d25_Part="210";
num_d100_Ampl="55";
num_d100_Full="1485";
num_d100_Part="1485";
num_FS_IDPs="1273";

# Regular IDPs
for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/IDP_files/IDPs.txt ] ; then
        echo $elem `cat $subjDir/$elem/IDP_files/IDPs.txt  | awk '{$1="" ; print $0}'` | sed 's|  | |g' 
    else    
        generate_NaNs $num_IDPs $elem
    fi
done > $CONFDATA/IDPs.txt &

# rfMRI connectivity - Dimensionality: 25
for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/fMRI/rfMRI_d25_NodeAmplitudes_v1.txt ] ; then
        echo "$elem `cat $subjDir/$elem/fMRI/rfMRI_d25_NodeAmplitudes_v1.txt`"
    else
        generate_NaNs $num_d25_Ampl $elem
    fi
done > $CONFDATA/rfMRI_d25_NodeAmplitudes_v1.txt &

for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/fMRI/rfMRI_d25_fullcorr_v1.txt ] ; then
        echo "$elem `cat $subjDir/$elem/fMRI/rfMRI_d25_fullcorr_v1.txt`"
    else
        generate_NaNs $num_d25_Full $elem
    fi
done > $CONFDATA/rfMRI_d25_fullcorr_v1.txt &

for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/fMRI/rfMRI_d25_partialcorr_v1.txt ] ; then
        echo "$elem `cat $subjDir/$elem/fMRI/rfMRI_d25_partialcorr_v1.txt`"
    else
        generate_NaNs $num_d25_Part $elem
    fi
done > $CONFDATA/rfMRI_d25_partialcorr_v1.txt &

# rfMRI connectivity - Dimensionality: 100
for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/fMRI/rfMRI_d100_NodeAmplitudes_v1.txt ] ; then
        echo "$elem `cat $subjDir/$elem/fMRI/rfMRI_d100_NodeAmplitudes_v1.txt`"
    else
        generate_NaNs $num_d100_Ampl $elem
    fi
done > $CONFDATA/rfMRI_d100_NodeAmplitudes_v1.txt &

for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/fMRI/rfMRI_d100_fullcorr_v1.txt ] ; then
        echo "$elem `cat $subjDir/$elem/fMRI/rfMRI_d100_fullcorr_v1.txt`"
    else
        generate_NaNs $num_d100_Full $elem
    fi
done > $CONFDATA/rfMRI_d100_fullcorr_v1.txt &

for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/fMRI/rfMRI_d100_partialcorr_v1.txt ] ; then
        echo "$elem `cat $subjDir/$elem/fMRI/rfMRI_d100_partialcorr_v1.txt`"
    else
        generate_NaNs $num_d100_Part $elem
    fi
done > $CONFDATA/rfMRI_d100_partialcorr_v1.txt &

paste $CONFDATA/rfMRI_d25_NodeAmplitudes_v1.txt \
      $CONFDATA/rfMRI_d100_NodeAmplitudes_v1.txt \
      $CONFDATA/rfMRI_d25_partialcorr_v1.txt \
      $CONFDATA/rfMRI_d100_partialcorr_v1.txt > $CONFDATA/rfMRI_IDPs.txt

# FS IDPs
for elem in `cat $CONFDATA/subj.txt` ; do 
    if [ -f $subjDir/$elem/IDP_files/FS_IDPs.txt ] ; then 
        echo $elem `cat $subjDir/$elem/IDP_files/FS_IDPs.txt  | awk '{$1="" ; print $0}'` | sed 's|  | |g' 
    else    
        generate_NaNs $num_FS_IDPs $elem
    fi
done > $CONFDATA/FS_IDPs.txt &


cp IDPinfo.txt $CONFDATA/
cp FS_headers_info.txt $CONFDATA/

