function my_BA_plot_no_python(P_S1, R_S1, P_S2, R_S2, N, ...
                    cols_names, rows_names, group_name, ...
                    cols_label, rows_label, shape_matrix, ...
                    general_name, PNG_dir, TXT_dir, HTML_dir,...
                    S1_text, S2_text, general_text)
                   
    label = strrep(group_name, '_', ' ');

    X = (P_S1 + P_S2) / 2;
    Y = P_S1 - P_S2;
    
    BA = [X, Y];

    IDP_indices=zeros(size(P_S1));
    COGN_indices=zeros(size(P_S1));

    f = figure('visible','off','PaperOrientation', 'landscape','Units', ...
               'points','Position', [0,0,1000,700]);
    dscatter(BA(:,1), BA(:,2)); hline = refline([0 0]);hline.Color = 'r'; 
    set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
    set(gca, 'FontSize',20);
    xlabel('Average of B & A', 'FontSize', 30);
    ylabel('B - A', 'FontSize', 30); 
    title({['Bland-Altman plot for ' label],  general_text, ...
           ['B = ' S1_text], ['A = ' S2_text]}, 'FontSize', 20);
    print('-dpng','-r70',[PNG_dir '/BA_' group_name ...
                          '_' general_name '.png']);
                               
end
