function [status, cmdout] = wait_for(job_ID)
    [status, cmdout] = system(['bb_wait_SGE_jobs ' strtrim(job_ID)]); 
end