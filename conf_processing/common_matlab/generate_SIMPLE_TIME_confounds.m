function [confSimple, namesSimple] = generate_SIMPLE_TIME_confounds(dataDir,...
                                                          ALL_IDs,indsite)
    %%% load FMRIB "internal" info
    N = length(ALL_IDs);
    FMRIB_info=load(strcat(dataDir, '/initial_workspace.txt'));

    [~,indices_IDPs,indices_FMRIB_info]=intersect(ALL_IDs,FMRIB_info(:,1)); 
    new_FMRIB_info=zeros(N,size(FMRIB_info,2))/0;  
    new_FMRIB_info(indices_IDPs,:)=FMRIB_info(indices_FMRIB_info,:);  
    FMRIB_info=new_FMRIB_info;
    clear new_FMRIB_info;

    % Get acquisition date
    timeStampY=floor(FMRIB_info(:,2)/10000); 
    timeStampM=floor((FMRIB_info(:,2)-timeStampY*10000)/100); 
    timeStampD=(FMRIB_info(:,2)-timeStampY*10000-timeStampM*100);

    numdays = datenum(timeStampY,timeStampM,timeStampD) - ...
               min(datenum(timeStampY,timeStampM,timeStampD));

    duplicatedDays = zeros(N,length(indsite));

    for i=1:length(indsite)
        medianForSite = nanmedian(numdays(indsite{i}));
        duplicatedDays(indsite{i},i) = numdays(indsite{i}) - medianForSite;
        normValueForSite = mad(duplicatedDays(indsite{i},i)) * 1.48;
        duplicatedDays(indsite{i},i) = duplicatedDays(indsite{i},i) ./ normValueForSite;
    end

    duplicatedDays(isnan(duplicatedDays)) = 0;
    duplicatedDays_squared = duplicatedDays.^2;

    confSimple = [duplicatedDays duplicatedDays_squared];

    namesSimple = {};

    for i=1:length(indsite)
        newName     = strcat('SimpleDate_Site_',num2str(i));
        namesSimple = [namesSimple ; newName];
    end
    for i=1:length(indsite)
        newName     = strcat('SimpleDate_Site_',num2str(i), '_Squared');
        namesSimple = [namesSimple ; newName];
    end
end
