function values = getValues(fileName, ALL_IDs)
    data=load(fileName);

    [~,indices_1,indices_2]=intersect(ALL_IDs,data(:,1));  
    values=zeros(length(ALL_IDs),size(data,2))/0;  
    values(indices_1,:)=data(indices_2,:);  

    values = values(:,2:end);
end
