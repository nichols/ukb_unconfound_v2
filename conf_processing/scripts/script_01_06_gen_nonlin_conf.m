%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');

rm_file('workspaces/ws_01/ws_01_04.mat');
make_dir('tables');
make_dir('figs/NONLIN/');

load('workspaces/ws_01/nonlin_confounds_generated.mat');

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After initial load');

total_veu=[];

num_IDPs = size(subset_IDPs_i,2);

for i =1:num_IDPs
    if mod(i,100) == 0
        my_log(fm, ['Loop: ' num2str(i)]);
    end

    namF = ['workspaces/ws_01/veu_nonlin/ws_' num2str(i) '.mat'];
    if exist(namF, 'file') ~=2
        my_log(fm, string(strcat('File', {' '}, namF, {' '}, ...
                        'does not exist. Regenerating.')));
        func_01_05_gen_nonlin_conf(i);
    end   
    
    load(strcat(namF));
    if size(veu) ~= [num_nonlin,1]
        func_01_05_gen_nonlin_confi(i);
        load(strcat(namF));
    end
    total_veu=[total_veu;veu'];
end

my_log(fm, 'After detailed load');

total_veu = total_veu';
new_names={};

for i=1:num_nonlin
    new_names{i} = names{ind_nonlin(1)+i-1};   
end

names_nonlin = new_names;

nbins = 200000;
limVE = 0.001;  % Limit to what is shown in the histogram.
V1=total_veu(:);
V1=find(V1>=limVE);
V=total_veu(V1);
numV = length(V);
red      = [190 0 0]   ./255;
blue     = [0 123 198] ./255;
darkblue = [0 62 99]   ./255;
teal     = [0.427 0.592 0.663];

f = figure('Visible', 'off','PaperOrientation', 'landscape', 'Units',...
           'points','Position', [0,0,1900,500]);
h=histogram(V, nbins);
set(gca, 'FontSize', 18);
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gcf, 'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'LooseInset', get(gca,'TightInset'));
yt=yticks;
ytl=yticklabels;
n=h.Values;
h.FaceColor=teal;
h.EdgeColor=teal;
ylim([-0.5,max(n)*2]);
hold on;
boxplot(V, 'Positions',10^(log10(max(n))/2), 'Orientation', 'horizontal',...
        'Widths', 10^(log10(max(n))/3));
yticks(yt);
yticklabels(ytl);
set(gca,'YScale', 'log');
set(findobj(gcf, 'tag', 'Median'), 'Color', red);
set(findobj(gcf, 'tag', 'Box'), 'Color', darkblue, 'linew', 1);
set(findobj(gcf, 'type', 'line'), 'linew', 1);
h1 = findobj(gcf,'tag', 'Outliers');
for iH = 1:length(h1)
    h1(iH).MarkerEdgeColor = blue;
end
ylim([-0.5,max(n)*2]);
cad1=strcat('Confounds x IDP with % UVE > ', num2str(limVE), ' (',...
            num2str(numV),' pairs)');
cad2=strcat(num2str(num_IDPs), {' '}, 'IDPs -', {' '},...
            num2str(num_nonlin), {' '}, ...
            'confounds - ',...
            {' ' }, num2str(nbins), {' '}, 'bins');
title({'Histogram + Boxplot for % UVE (log scale in X and Y):',...
                                                            cad1,cad2{1}});
ylabel('Log_1_0 Frequency');
xlabel('Log_1_0 % Unique Variance Explained');
print('-dpng','-r70','figs/NONLIN/hist_log_ve_0.001.png');



f = figure('Visible', 'off','PaperOrientation', 'landscape', 'Units',...
           'points','Position', [0,0,800,500]);
violinplot(log10(V),'Non-linear confounds', 'ShowData', false);
set(gca, 'FontSize', 18);
set(gcf, 'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'LooseInset', get(gca,'TightInset'));
cad1=strcat('Confounds x IDP with % UVE > ', num2str(limVE), ' (',...
            num2str(numV),' pairs)');
cad2=strcat(num2str(num_IDPs), {' '}, 'IDPs -', {' '},...
            num2str(num_nonlin), {' '}, 'confounds');
title({'Violin Plot for % UVE:',cad1,cad2{1}});
ylabel('Log_1_0 % Unique Variance Explained');
hold on;
print('-dpng','-r70','figs/NONLIN/violin_ve_0.001.png');


% Averaging across IDPs:
names2={};
for i=1:num_nonlin
    names2{i} = strrep(names_nonlin{i}, '_', ' ');
end

avg_VE = zeros(num_nonlin,1);
for i=1:num_nonlin
    avg_VE(i) = nanmean(total_veu(i,:));
end

max_VE = zeros(num_nonlin,1);
for i=1:num_nonlin
    max_VE(i) = nanmax(total_veu(i,:));
end

thr_for_avg = prctile(avg_VE,95);
thr_for_UVE = max(0.75, prctile(total_veu(:),99.9));

rm_make_dir('TXT_DATA/NONLIN');
rm_make_dir('HTML/NONLIN');

dlmwrite(['TXT_DATA/NONLIN/NONLIN_data.txt'], total_veu, 'delimiter', ' ');
dlmwrite(['TXT_DATA/NONLIN/NONLIN_thrs.txt'], [thr_for_avg, thr_for_UVE], ...
                                                        'delimiter', ' ');

fileName=['TXT_DATA/NONLIN/NONLIN_names.txt'];
fileNLs=fopen(fileName,'w');
fprintf(fileNLs,'%s\n', names_nonlin{:});
fclose(fileNLs);

fileName=['TXT_DATA/NONLIN/IDPs_names.txt'];
fileIDPs=fopen(fileName,'w');
fprintf(fileIDPs,'%s\n', IDPnames{:});
fclose(fileIDPs);


f = figure('Visible', 'off', 'PaperOrientation', 'landscape', 'Units',...
           'points','Position', [0,0,2000,500]);

plot([1:num_nonlin], avg_VE, 'LineWidth',3, 'Color', [0 0 1]);
hline = refline([0 thr_for_avg]);
hline.Color='r';
hline.LineWidth=2;
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca,'LooseInset', get(gca,'TightInset'));
xlim([-3,num_nonlin+1]);
ylabel('% Unique Variance Explained');
xlabel('Confound');
grid on;
set(gca, 'FontSize',18);
title(['Mean (across IDPs) % of Unique Variance Explained per conf - Threshold: ' ...
        num2str(thr_for_avg)]);
legend('Mean');
print('-dpng','-r70','figs/NONLIN/mean_UVE_across_IDPs.png');



f = figure('Visible', 'off', 'PaperOrientation', 'landscape', 'Units',...
           'points','Position', [0,0,2000,500]);

plot([1:num_nonlin], max_VE, 'LineWidth',3, 'Color', [0 0 1]);
hline = refline([0 thr_for_UVE]);
hline.Color='r';
hline.LineWidth=2;
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca,'LooseInset', get(gca,'TightInset'));
xlim([-3,num_nonlin+1]);
ylabel('% Unique Variance Explained');
xlabel('Confound');
grid on;
set(gca, 'FontSize',18);
title(['Max (across IDPs) % of UVE per confound - Threshold: ' ...
                                                    num2str(thr_for_UVE)]);
legend('Max');
print('-dpng','-r70','figs/NONLIN/max_UVE_across_IDPs.png');

final_nonlin_conf_list = [];

% Generating table with confounds that pass the first threshold
% First threshold is based on mean (across IDPs) of the UVE.

inds_for_avg = find(avg_VE > thr_for_avg);

fileID=fopen('tables/mean_UVE_nonlin.txt','w');

for i=1:length(inds_for_avg)
    j = inds_for_avg(i);
    fprintf(fileID,'%s %.6f\n',names_nonlin{j},avg_VE(j));
end

final_nonlin_conf_list = inds_for_avg;

% Generating table with confounds that pass the second threshold
% Second threshold is based purely on the UVE.
inds_for_UVE = find(total_veu > thr_for_UVE);

fileID=fopen('tables/UVE_nonlin.txt','w');

for i=1:length(inds_for_UVE)
    [conf_ind,IDP_ind]=ind2sub(size(total_veu), inds_for_UVE(i));
    final_nonlin_conf_list = [final_nonlin_conf_list; conf_ind];
    IDP_name = strrep(IDPnames{IDP_ind}, ' ', '_');
    fprintf(fileID,'%s %s %.6f\n',names_nonlin{conf_ind}, IDP_name, ...
                                              total_veu(conf_ind,IDP_ind));
end

% Generating Manhattan plot


IDP_group_dir={};
IDP_group_files={};
num_IDP_groups={};
ind_IDP_groups={};
IDP_group_dir{1} = '../data/GROUPS_IDPs_7_groups/';
IDP_group_dir{2} = '../data/GROUPS_IDPs_all/';

for i=1:length(IDP_group_dir)
    IDP_group_files{i}= dir(strcat(IDP_group_dir{i}, '*.txt'));
    num_IDP_groups{i} = length(IDP_group_files{i});
    ind_IDP_groups{i} = [];
end

for i=1:length(IDP_group_dir)
    for j = 1:num_IDP_groups{i}
        fileID = fopen(strcat(IDP_group_dir{i}, ...
                       IDP_group_files{i}(j).name),'r');    
        tmp=fscanf(fileID, '%i');
        ind_IDP_groups{i}(j).name = IDP_group_files{i}(j).name;
        ind_IDP_groups{i}(j).ind  = tmp;
    end
end


numVals=num_nonlin;
maxV=[];
indi=[];
maxVMat=[];
indiMat=[];
cont=1;
for i=1:num_IDPs
    temp=sort(total_veu(:,i));
    indi=[indi;ones(numVals,1)*cont];
    indiMat=[indiMat;ones(1,numVals)*cont];
    maxV=[maxV;temp(end:-1:end-numVals+1)'];
    maxVMat=[maxVMat;temp(end:-1:end-numVals+1)];
    cont=cont+1;
end

le={};
f = figure('Visible', 'off','PaperOrientation', 'landscape', 'Units',...
            'points','Position', [0,0,2000,500]);
cOrder=get(gca,'ColorOrder');
cOrder(8,:)=cOrder(1,:);
cOrder(9,:)=cOrder(3,:);

correctOrder=[2,3,1,7,4,5,6,3];
cont=1;
h=[];
for i = correctOrder
    new_maxV=[];
    new_indi=[];
    newIndices=ind_IDP_groups{1}(i).ind;
    na=strrep(ind_IDP_groups{1}(i).name,'_',' ');
    na=strrep(na,'.txt','');
    le{cont}=na;
    for k=newIndices(:)'
        new_indi=[new_indi;ones(numVals,1).*k];
        temp=sort(total_veu(:,k));
        new_maxV=[new_maxV;temp(end:-1:end-numVals+1)];
    end

    h(cont)=plot(new_indi,new_maxV,'.','MarkerSize',10, ...
                                   'MarkerEdgeColor',cOrder(cont+1,:) );
    hold on;
    cont=cont+1;
end
en=[];
for i = 1:num_IDP_groups{1}
   en(i)=ind_IDP_groups{1}(i).ind(end) ;
end

hline = refline([0 thr_for_UVE]);
hline.Color = 'r';
hline.LineWidth=2;

xticks([0,sort(en)]);
xlim([-20,num_IDPs+20]);
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'FontSize',18);
set(gca, 'LooseInset', get(gca, 'TightInset'));
title(strcat('% UVE of each IDP (', num2str(num_IDPs) ...
            ,') by each non-linear confound (', num2str(num_nonlin)...
            ,') - Threshold: ', {' '}, num2str(thr_for_UVE)));
xticklabels('')

ile={};
for il = 1:7
    ile{il}=le{il};
end

hleg = legend(h(1:end-1),ile,'FontSize',20);
ylabel('% Variance Explained');
xlabel('IDPs');
grid on;
print('-dpng','-r70',strcat('figs/NONLIN/manhattan1.png'));

clear f;
clear gca;
clear gcf;

comm = ['./scripts/script_01_06_gen_nonlin_conf.py NONLIN_data ',...
         'NONLIN_thrs IDPs_names NONLIN_names ' ...
         '../data/GROUPS_IDPs_7_groups/ TXT_DATA/NONLIN/ ', ...
         'HTML/NONLIN/ non-linear'];
[status, cmdout] = system(comm);

clear maxV;
clear maxVMat;

% Saving workspace
save('workspaces/ws_01/ws_01_04.mat', '-v7.3')
my_log(fm, 'End');
