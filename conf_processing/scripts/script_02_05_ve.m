%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');

rm_file('workspaces/ws_02/ve_04.mat');
load('workspaces/ws_02/confounds_generated.mat');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

nCONFg = length(ind_conf_groups);

num_IDP=size(subset_IDPs_i,2);
num_confounds=size(conf,2);

variance_explained_i                 = {};
variance_explained_i_uniq            = {};
variance_explained_Rand_i            = {};
variance_explained_Rand_i_uniq       = {};
variance_explained_RandGroups_i      = {};
variance_explained_RandGroups_i_uniq = {};
variance_explained_Simple            = {};
variance_explained_PCA               = {};
variance_explained_PCA90             = {};
variance_explained_PCA99             = {};
variance_explained_Rand_SIMPLE       = {};
variance_explained_Rand_SIMPLE_uniq  = {};

for i = 1:nCONFg

    variance_explained_i{i}                 = {};
    variance_explained_i_uniq{i}            = {};
    variance_explained_Rand_i{i}            = {};
    variance_explained_Rand_i_uniq{i}       = {};
    variance_explained_RandGroups_i{i}      = {};
    variance_explained_RandGroups_i_uniq{i} = {};
    variance_explained_Simple{i}            = [];
    variance_explained_PCA{i}               = [];
    variance_explained_PCA90{i}             = [];
    variance_explained_PCA99{i}             = [];
    variance_explained_Rand_SIMPLE{i}       = [];
    variance_explained_Rand_SIMPLE_uniq{i}  = [];
end

for i = 1:nCONFg
    variance_explained_i{i}{length(num_groups)}                 = [];
    variance_explained_i_uniq{i}{length(num_groups)}            = [];
    variance_explained_Rand_i{i}{length(num_groups)}            = [];
    variance_explained_Rand_i_uniq{i}{length(num_groups)}       = [];
    variance_explained_RandGroups_i{i}{length(num_groups)}      = [];
    variance_explained_RandGroups_i_uniq{i}{length(num_groups)} = [];
end

variance_explained_i_all   = [];

for I = 1:nCONFg
    for i =1:num_IDP
        if mod(i,50) == 0
            my_log(fm, ['Loop: ' num2str(I) ' / ' num2str(i)]);
        end
        namF = strcat('workspaces/ws_02/ve/ws_',num2str(i),'.mat');
        if exist(namF, 'file') ~=2
            my_log(fm, string(strcat('File', {' '}, namF, {' '}, ...
                        'does not exist. Regenerating.')));
            func_02_04_ve(i);
        end   
        load(namF);
        for k = 1:num_groups(I)
            variance_explained_i{I}{k}(i)      = finalValues.ve_i{I}(k);
            variance_explained_i_uniq{I}{k}(i) = finalValues.veu_i{I}(k);
            variance_explained_RandGroups_i{I}{k}(i)      = finalValues.ve_RandGroups_i{I}(k);
            variance_explained_RandGroups_i_uniq{I}{k}(i) = finalValues.veu_RandGroups_i{I}(k);

        end
        for k = 1:num_groups_Rand
            variance_explained_Rand_i{I}{k}(i)      = finalValues.veRand_i{I}(k);
            variance_explained_Rand_i_uniq{I}{k}(i) = finalValues.veuRand_i{I}(k);
        end
        variance_explained_i_all{I}(i)            = finalValues.ve_all(I);
        variance_explained_Simple{I}(i)           = finalValues.ve_Simple{I};
        variance_explained_PCA{I}(i)              = finalValues.ve_PCA{I};
        variance_explained_PCA90{I}(i)            = finalValues.ve_PCA90{I};
        variance_explained_PCA99{I}(i)            = finalValues.ve_PCA99{I};                
        variance_explained_Rand_SIMPLE{I}(i)      = finalValues.ve_Rand_SIMPLE{I};
        variance_explained_Rand_SIMPLE_uniq{I}(i) = finalValues.veu_Rand_SIMPLE{I};
        variance_explained_Rand_PCA{I}(i)         = finalValues.ve_Rand_PCA{I};
        variance_explained_Rand_PCA_uniq{I}(i)    = finalValues.veu_Rand_PCA{I};
        variance_explained_Rand_PCA90{I}(i)       = finalValues.ve_Rand_PCA90{I};
        variance_explained_Rand_PCA90_uniq{I}(i)  = finalValues.veu_Rand_PCA90{I};
        variance_explained_Rand_PCA99{I}(i)       = finalValues.ve_Rand_PCA99{I};
        variance_explained_Rand_PCA99_uniq{I}(i)  = finalValues.veu_Rand_PCA99{I};

    end

    for k = 1:num_groups(I)
        variance_explained_i{I}{k}=variance_explained_i{I}{k}';
        variance_explained_i_uniq{I}{k}=variance_explained_i_uniq{I}{k}';
        variance_explained_RandGroups_i{I}{k}=variance_explained_RandGroups_i{I}{k}';
        variance_explained_RandGroups_i_uniq{I}{k}=variance_explained_RandGroups_i_uniq{I}{k}';

    end
    for k = 1:num_groups_Rand
        variance_explained_Rand_i{I}{k}=variance_explained_Rand_i{I}{k}';
        variance_explained_Rand_i_uniq{I}{k}=variance_explained_Rand_i_uniq{I}{k}';
    end
end

clear maxV;
clear maxVMat;
clear c;
clear na;
clear found90;
clear fid;
clear fileID;
clear fileIDPs;
clear fileNLs;
clear filePH;
clear h1;
clear hleg; 
clear hline;
clear ans;
clear a;
clear b;
clear f;
clear h;
clear i;
clear I;
clear j;
clear k;
clear l;
clear s;
clear iH;
clear il;
clear limVE;
clear n;

save('workspaces/ws_02/ve_04.mat', '-v7.3')
my_log(fm, 'End');
