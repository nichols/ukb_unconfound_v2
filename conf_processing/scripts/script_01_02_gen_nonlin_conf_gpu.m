%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Loads the raw confounds generated in the previous stage and          %
% then generates non-linear version of the non-categorical confounds.  %
% Generated confounds are stored in workspaces/confounds_generated.mat %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');

load('workspaces/ws_01/raw_confounds_generated.mat');
finalFile = 'workspaces/ws_01/nonlin_confounds_generated.mat';

rm_file(finalFile);
rm_make_dir('workspaces/ws_01/veu_nonlin/');
rm_make_dir('tables');

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

make_dir('figs/CORR/');
plotNames={'NORMAL', 'REDUCED'};



conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC};        
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;namesSite;namesBatch;...
         namesCMRR;namesProtocol;namesServicePack;namesScanRamp;...
         namesScanColdHead;namesScanHeadCoil;namesScanMisc;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC];        

group_Names = {{}};
group_Names{1} = {'AGE', 'SEX', 'AGE_SEX', 'HEAD_SIZE', 'SITE', 'BATCH', ...
                  'CMRR', 'PROTOCOL', 'SERVICE_PACK', 'SCAN_RAMP', ...
                  'SCAN_COLD_HEAD', 'SCAN_HEAD_COIL', 'SCAN_MISC', ...
                  'FLIPPED_SWI', 'FS_T2', 'NEW_EDDY', 'SCALING', 'TE', ...
                  'STRUCT_MOTION', 'DVARS', 'HEAD_MOTION', 'HEAD_MOTION_ST', ...
                  'TABLE_POS', 'EDDY_QC'};   
     
num_conf      = {};
num_groups    = [];
num_conf{1}   = size(conf,2);
num_groups(1) = length(conf_Group);
ind_no_nonlin = 1:size(conf,2);

%This will allow for multiple confound groupings.
ind_conf_groups    = {{}};

previous = 0;
for i = 1:num_groups(1)
    num_elements=size(conf_Group{i},2);
    ind_conf_groups{1}{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates an alternative way of ordering the confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind_conf_groups{2} = {};

% Subject specific confounds
ind_conf_groups{2}{1}=[ind_conf_groups{1}{1}, ind_conf_groups{1}{2}, ...
                       ind_conf_groups{1}{3}, ind_conf_groups{1}{4}];

% Acquisition / scanner related confounds
ind_conf_groups{2}{2}=[ind_conf_groups{1}{5},  ind_conf_groups{1}{6}, ...
                       ind_conf_groups{1}{7},  ind_conf_groups{1}{8}, ...
                       ind_conf_groups{1}{9},  ind_conf_groups{1}{10},...
                       ind_conf_groups{1}{11}, ind_conf_groups{1}{12},...
                       ind_conf_groups{1}{13}, ind_conf_groups{1}{14},...
                       ind_conf_groups{1}{15}, ind_conf_groups{1}{16},...
                       ind_conf_groups{1}{17}, ind_conf_groups{1}{18}];

% Motion confounds
ind_conf_groups{2}{3}=[ind_conf_groups{1}{19}, ind_conf_groups{1}{20},... 
                       ind_conf_groups{1}{21}, ind_conf_groups{1}{22}];

% Table Position confounds
ind_conf_groups{2}{4}=[ind_conf_groups{1}{23}, ind_conf_groups{1}{24}];

num_groups(2)  = length(ind_conf_groups{2});
group_Names{2} = {'SUBJECT', 'ACQ', 'MOTION', 'TABLE'};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generation of non-linear confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[confAge_nonlin,              namesAge_nonlin] =              generate_nonlin(conf(:, ind_conf_groups{1}{1}), names(ind_conf_groups{1}{1}),  conf, indSite);
[confAgeSex_nonlin,           namesAgeSex_nonlin] =           generate_nonlin(conf(:, ind_conf_groups{1}{3}), names(ind_conf_groups{1}{3}),  conf, indSite);
[confHEADSIZE_nonlin,         namesHEADSIZE_nonlin] =         generate_nonlin(conf(:, ind_conf_groups{1}{4}), names(ind_conf_groups{1}{4}),  conf, indSite);
[confTE_nonlin,               namesTE_nonlin] =               generate_nonlin(conf(:, ind_conf_groups{1}{18}),names(ind_conf_groups{1}{18}), conf, indSite);
[confSTRUCTHEADMOTION_nonlin, namesSTRUCTHEADMOTION_nonlin] = generate_nonlin(conf(:, ind_conf_groups{1}{19}),names(ind_conf_groups{1}{19}), conf, indSite);
[confDVARS_nonlin,            namesDVARS_nonlin] =            generate_nonlin(conf(:, ind_conf_groups{1}{20}),names(ind_conf_groups{1}{20}), conf, indSite);
[confHEADMOTION_nonlin,       namesHEADMOTION_nonlin] =       generate_nonlin(conf(:, ind_conf_groups{1}{21}),names(ind_conf_groups{1}{21}), conf, indSite);
[confHEADMOTIONST_nonlin,     namesHEADMOTIONST_nonlin] =     generate_nonlin(conf(:, ind_conf_groups{1}{22}),names(ind_conf_groups{1}{22}), conf, indSite);
[confTABLEPOS_nonlin,         namesTABLEPOS_nonlin] =         generate_nonlin(conf(:, ind_conf_groups{1}{23}),names(ind_conf_groups{1}{23}), conf, indSite);
[confEDDYQC_nonlin,           namesEDDYQC_nonlin] =           generate_nonlin(conf(:, ind_conf_groups{1}{24}),names(ind_conf_groups{1}{24}), conf, indSite);


clear conf conf_Group names;

conf_nonlin = [confAge_nonlin confAgeSex_nonlin confHEADSIZE_nonlin ...
    confTE_nonlin confSTRUCTHEADMOTION_nonlin confDVARS_nonlin ...
    confHEADMOTION_nonlin confHEADMOTIONST_nonlin confTABLEPOS_nonlin ...
    confEDDYQC_nonlin];

names_nonlin = [namesAge_nonlin;namesAgeSex_nonlin;namesHEADSIZE_nonlin;...
    namesTE_nonlin;namesSTRUCTHEADMOTION_nonlin;namesDVARS_nonlin;...
    namesHEADMOTION_nonlin;namesHEADMOTIONST_nonlin;namesTABLEPOS_nonlin;...
    namesEDDYQC_nonlin];

conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC conf_nonlin];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC,conf_nonlin};        
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;namesSite;namesBatch;...
         namesCMRR;namesProtocol;namesServicePack;namesScanRamp;...
         namesScanColdHead;namesScanHeadCoil;namesScanMisc;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC;names_nonlin];        

group_Names = {{}};
group_Names{1} = {'AGE', 'SEX', 'AGE_SEX', 'HEAD_SIZE', 'SITE', 'BATCH', ...
                  'CMRR', 'PROTOCOL', 'SERVICE_PACK', 'SCAN_RAMP', ...
                  'SCAN_COLD_HEAD', 'SCAN_HEAD_COIL', 'SCAN_MISC', ...
                  'FLIPPED_SWI', 'FS_T2', 'NEW_EDDY', 'SCALING', 'TE', ...
                  'STRUCT_MOTION', 'DVARS', 'HEAD_MOTION', 'HEAD_MOTION_ST', ...
                  'TABLE_POS', 'EDDY_QC', 'NON_LIN'};   

num_conf      = {};
num_groups    = [];
num_conf{1}   = size(conf,2);
num_groups(1) = length(conf_Group);

%This will allow for multiple confound groupings.
ind_conf_groups    = {{}};

previous = 0;
for i = 1:num_groups(1)
    num_elements=size(conf_Group{i},2);
    ind_conf_groups{1}{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end

clear conf_Group;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the indices in the conf for each element in conf_Group %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IDP_group_dir={};
IDP_group_dir{1} = '../data/GROUPS_IDPs_7_groups/';
IDP_group_dir{2} = '../data/GROUPS_IDPs_all/';

for i=1:length(IDP_group_dir)
    IDP_group_files{i}= dir(strcat(IDP_group_dir{i}, '*.txt'));
    num_IDP_groups{i} = length(IDP_group_files{i});
    ind_IDP_groups{i} = [];
end

for i=1:length(IDP_group_dir)
    for j = 1:num_IDP_groups{i}
        fileID = fopen(strcat(IDP_group_dir{i}, ...
                       IDP_group_files{i}(j).name),'r');    
        tmp=fscanf(fileID, '%i');
        ind_IDP_groups{i}(j).name = IDP_group_files{i}(j).name;
        ind_IDP_groups{i}(j).ind  = tmp;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates an alternative way of ordering the confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Non-linear confounds
ind_conf_groups{2}{5}=[ind_conf_groups{1}{size(ind_conf_groups{1},2)}];

group_Names{2} = {'SUBJECT', 'ACQ', 'MOTION', 'TABLE', 'NONLIN'};
num_groups(2)=length(group_Names{2});


%%%%%%%%%%%%
% CLEANING %
%%%%%%%%%%%%
clear conf_Non_Temporal;
clear all_PCA;
clear ESM;
clear HE;
clear AbyB;
clear BbyA;
clear ans;
clear Princ_Components_st;
clear indSite_st;
clear cont;
clear max_VE;
clear numDateComponents;
clear previous;
clear denominator;
clear gausskernel;
clear headMotion;
clear i;
clear j;
clear k;
clear numerator;
clear princ_components_cell;
clear princ_components_cell_st;
clear total_princ_components_cell;
clear total_ESM;
clear loaded_smoothed_subset_IDPs_st;
clear loaded_smoothed_subset_IDPs;
clear subset_IDPs_i_st;
clear subset_IDPs_i_deconf_st;
clear total_princ_components_cell_st;
clear numTempComponents;
clear unsorted_index_sortedTime;
clear unsorted_index_sortedDate;
clear new_index_sortedTime;
clear new_index_sortedDate;
clear index_sortedDate;
clear index_sortedTime;
clear sortedTime;
clear scan_date;
clear scan_date_cont;
clear dataDir;
clear sigma;
clear tmp;


my_log(fm, 'After load');

save('workspaces/ws_01/IDPs.mat', '-v7.3','subset_IDPs');

clear i;
clear j;
clear f;
clear I;
clear new_i;
clear new_j;
clear mat;
clear group_Names_corr;
clear new_group_Names;
clear new_conf_Group;
clear subset_IDPs;
clear ALL_IDPs;
clear IDPs_for_Site;
clear Princ_Components;
clear subset_IDPs_i_deconf;


ind_nonlin = size(ind_no_nonlin,2) + 1:size(conf,2);
ind_conf = 1:size(conf,2);

num_nonlin = size(ind_nonlin,2);
num_no_nonlin = size(ind_no_nonlin,2);

fileID = fopen('tables/summary.txt', 'w');
fprintf(fileID, 'Initial Number of NONLINs: %s\n', num2str(num_nonlin));
fclose(fileID);


my_log(fm, 'Before unconfounding');

IDP_i_deconf  = nets_unconfound2(subset_IDPs_i, conf(:,ind_no_nonlin));
    
my_log(fm, 'After unconfounding');

save('workspaces/ws_01/IDPs_i.mat', '-v7.3','subset_IDPs_i');
save('workspaces/ws_01/IDPs_i_nonlin_deconf.mat', '-v7.3', 'IDP_i_deconf');

clear IDP_i_deconf;
clear IDP_i;

save(finalFile, '-v7.3');
my_log(fm, 'End');
