%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
if exist('workspaces/ws_03/', 'dir') ~= 2
    mkdir('workspaces/ws_03/');
end
if exist('workspaces/ws_03/ws_03_subset_IDPs_i.mat', 'file') == 2
    delete('workspaces/ws_03/ws_03_subset_IDPs_i.mat');
end
if exist('workspaces/ws_03/ws_03_BODY_vars_i.mat', 'file') == 2
    delete('workspaces/ws_03/ws_03_BODY_vars_i.mat');
end
if exist('workspaces/ws_03/ws_03_COGN_vars_i.mat', 'file') == 2
    delete('workspaces/ws_03/ws_03_COGN_vars_i.mat');
end
if exist('workspaces/ws_03/ws_03_conf.mat', 'file') == 2
    delete('workspaces/ws_03/ws_03_conf.mat');
end
load('workspaces/ws_02/ve_04.mat');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

% Normalizing the non-IDP variables and unconfound
BODY_vars_i = nets_inormal(BODY_vars);
COGN_vars_i = nets_inormal(COGN_vars);

subset_IDPs_i = gather(subset_IDPs_i);
BODY_vars_i = gather(BODY_vars_i);
COGN_vars_i = gather(COGN_vars_i);
conf = gather(conf);

mkdir('workspaces/ws_03/');
mkdir('workspaces/ws_03/IDP_deconf_04/');
mkdir('workspaces/ws_03/BODY_vars_i_deconf_05/');
mkdir('workspaces/ws_03/COGN_vars_i_deconf_05/');

save('workspaces/ws_03/ws_03_subset_IDPs_i.mat', '-v7.3','subset_IDPs_i');
save('workspaces/ws_03/ws_03_BODY_vars_i.mat', '-v7.3','BODY_vars_i');
save('workspaces/ws_03/ws_03_COGN_vars_i.mat', '-v7.3','COGN_vars_i');
save('workspaces/ws_03/ws_03_conf.mat', '-v7.3','conf');

my_log(fm, 'End');
