%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Loads the raw confounds generated in the previous stage and          %
% then generates non-linear version of the non-categorical confounds.  %
% Generated confounds are stored in workspaces/confounds_generated.mat %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
load('workspaces/ws_01/ws_01_16.mat');

dataDir = strcat('../data/', dataName, '_data/');
make_dir('workspaces/ws_02/');

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC conf_nonlin conf_ct confAcqTime ...
              confAcqDate];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC,conf_nonlin,conf_ct,confAcqTime,...
              confAcqDate};        
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;namesSite;namesBatch;...
         namesCMRR;namesProtocol;namesServicePack;namesScanRamp;...
         namesScanColdHead;namesScanHeadCoil;namesScanMisc;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC;names_nonlin;names_ct;namesAcqTime;...
         namesAcqDate];        

group_Names = {{}};
group_Names{1} = {'AGE', 'SEX', 'AGE_SEX', 'HEAD_SIZE', 'SITE', 'BATCH', ...
                  'CMRR', 'PROTOCOL', 'SERVICE_PACK', 'SCAN_RAMP', ...
                  'SCAN_COLD_HEAD', 'SCAN_HEAD_COIL', 'SCAN_MISC', ...
                  'FLIPPED_SWI', 'FS_T2', 'NEW_EDDY', 'SCALING', 'TE', ...
                  'STRUCT_MOTION', 'DVARS', 'HEAD_MOTION', 'HEAD_MOTION_ST', ...
                  'TABLE_POS', 'EDDY_QC', 'NON_LIN' 'CROSSED_TERMS', ...
                  'ACQ_TIME', 'ACQ_DATE'}; 

num_conf      = {};
num_groups    = [];
num_conf{1}   = size(conf,2);
num_groups(1) = length(conf_Group);

%This will allow for multiple confound groupings.
ind_conf_groups    = {{}};

previous = 0;
for i = 1:num_groups(1)
    num_elements=size(conf_Group{i},2);
    ind_conf_groups{1}{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end

clear conf_Group;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE SIMPLE CONFOUNDS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[confSimpleDate, namesSimpleDate] = generate_SIMPLE_TIME_confounds(dataDir,...
                                                        ALL_IDs, indSite);

ind_HM = [];
for i=1:length(namesHeadMotion)
    if ~isempty(strfind(namesHeadMotion{i}, ...
                'HeadMotion_mean_tfMRI_rel_Site_'))
        ind_HM = [ind_HM,i];
    end
    if ~isempty(strfind(namesHeadMotion{i}, ...
                'HeadMotion_mean_rfMRI_rel_Site_'))
        ind_HM = [ind_HM,i];
    end
end                                                   
                
simpleNamesHeadMotion = namesHeadMotion(ind_HM);

conf_Simple = [confAge(:,1:end) confAge_nonlin(:,1:numSites), ...
               confSex(:,1:end) confAgeSex(:,1:end) ...
               confHeadSize(:,1:end) confSite ...
               confHeadMotion(:,ind_HM) confSimpleDate(:,1:end)];

names_Simple = {namesAge(1:end) namesAge_nonlin(1:numSites) ...
                namesSex(1:end) namesAgeSex(1:end) ...
                namesHeadSize(1:end) namesSite ...
                simpleNamesHeadMotion namesSimpleDate(1:end)};

group_Names_Simple = {'SIMPLE'};
num_conf_Simple  = size(conf_Simple,2);

clear confSimpleDate;
clear namesSimpleDate;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE PCA CONFOUNDS %
%%%%%%%%%%%%%%%%%%%%%%%%%%
[all_PCA,ESM,HE]=nets_svds(conf,0);
conf_PCA = all_PCA(:, 1:num_conf_Simple);

% We start with a minimum of the PCA we had before
numPCA90 = num_conf_Simple;
numPCA99 = num_conf_Simple;
found90 = false;
max_VE = 0;
if check_GPU()
    conf = gpuArray(conf);
end

while(max_VE < 99)
    if mod(numPCA99, 10) == 0
        my_log(fm,[num2str(numPCA99) ' / ' num2str(max_VE)]);
    end
    if check_GPU()
        PCs = gpuArray(all_PCA(:,1:numPCA99));
    else
        PCs = all_PCA(:,1:numPCA99);
    end
        
    [AbyB,BbyA] = space_spanned(PCs,conf);

    max_VE = BbyA;
    if ~found90
        if max_VE > 90
           numPCA90 = numPCA99;
           found90 = true;
        end
    end
   
    if max_VE < 99
        numPCA99=numPCA99+1;
    end
end

if check_GPU()
   conf = gather(conf); 
end

clear AbyB BbyA max_VE PCs;
conf_PCA90 = all_PCA(:, 1:numPCA90);
conf_PCA99 = all_PCA(:, 1:numPCA99);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE RANDOM CONFOUNDS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
confR = generate_RANDOM_confounds(N,size(conf,2),num_conf_Simple, ...
                                    numPCA90, numPCA99);

confR001    = confR.confR001;
confR010    = confR.confR010;
confR100    = confR.confR100;
confRMAX    = confR.confRMAX;
confRSIM    = confR.confRSIM;
confRPCA    = confR.confRPCA;
confRPCA90  = confR.confRPCA90;
confRPCA99  = confR.confRPCA99;
namesR001   = confR.namesR001;
namesR010   = confR.namesR010;
namesR100   = confR.namesR100;
namesRMAX   = confR.namesRMAX;
namesRSIM   = confR.namesRSIM;
namesRPCA   = confR.namesRPCA;
namesRPCA90 = confR.namesRPCA90;
namesRPCA99 = confR.namesRPCA99;

clear confR;

conf_Rand        = [confR001 confR010 confR100 confRMAX];
conf_Group_Rand  = {confR001,confR010,confR100,confRMAX};

group_Names_Rand = {'RAND001','RAND010','RAND100','RANDMAX'};
names_Rand       = vertcat(namesR001,namesR010,namesR100,namesRMAX)';

num_conf_Rand    = size(conf_Rand,2);
num_groups_Rand  = length(group_Names_Rand);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the indices in the conf for each element in conf_Group %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind_conf_groups_Rand = {};
previous = 0;
for i = 1:num_groups_Rand
    num_elements=size(conf_Group_Rand{i},2);
    ind_conf_groups_Rand{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end

IDP_group_dir={};
IDP_group_dir{1} = '../data/GROUPS_IDPs_7_groups/';
IDP_group_dir{2} = '../data/GROUPS_IDPs_all/';

for i=1:length(IDP_group_dir)
    IDP_group_files{i}= dir(strcat(IDP_group_dir{i}, '*.txt'));
    num_IDP_groups{i} = length(IDP_group_files{i});
    ind_IDP_groups{i} = [];
end

for i=1:length(IDP_group_dir)
    for j = 1:num_IDP_groups{i}
        fileID = fopen(strcat(IDP_group_dir{i}, ...
                       IDP_group_files{i}(j).name),'r');    
        tmp=fscanf(fileID, '%i');
        ind_IDP_groups{i}(j).name = IDP_group_files{i}(j).name;
        ind_IDP_groups{i}(j).ind  = tmp;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates an alternative way of ordering the confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind_conf_groups{2} = {};

% Subject specific confounds
ind_conf_groups{2}{1}=[ind_conf_groups{1}{1}, ind_conf_groups{1}{2}, ...
                       ind_conf_groups{1}{3}, ind_conf_groups{1}{4}];

% Acquisition / scanner related confounds
ind_conf_groups{2}{2}=[ind_conf_groups{1}{5},  ind_conf_groups{1}{6}, ...
                       ind_conf_groups{1}{7},  ind_conf_groups{1}{8}, ...
                       ind_conf_groups{1}{9},  ind_conf_groups{1}{10},...
                       ind_conf_groups{1}{11}, ind_conf_groups{1}{12},...
                       ind_conf_groups{1}{13}, ind_conf_groups{1}{14},...
                       ind_conf_groups{1}{15}, ind_conf_groups{1}{16},...
                       ind_conf_groups{1}{17}, ind_conf_groups{1}{18}];

% Motion confounds
ind_conf_groups{2}{3}=[ind_conf_groups{1}{19}, ind_conf_groups{1}{20},... 
                       ind_conf_groups{1}{21}, ind_conf_groups{1}{22}];

% Table Position confounds
ind_conf_groups{2}{4}=[ind_conf_groups{1}{23}, ind_conf_groups{1}{24}];

% Non-linear confounds
ind_conf_groups{2}{5}=[ind_conf_groups{1}{25}];

% Crossed-term confounds
ind_conf_groups{2}{6}=[ind_conf_groups{1}{26}];

% Acquisiton Time / Date confounds
ind_conf_groups{2}{7}=[ind_conf_groups{1}{27}, ind_conf_groups{1}{28}];

num_groups(2)=7;
group_Names{2} = {'SUBJECT', 'ACQ', 'MOTION', 'TABLE', 'NON_LIN', ...
                  'CROSSED_TERMS','TIME_DATE',};
              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates an alternative way of ordering the confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind_conf_groups{3} = {};

% Subject specific confounds
ind_conf_groups{3}{1}=[ind_conf_groups{1}{1}];
ind_conf_groups{3}{2}=[ind_conf_groups{1}{2}];
ind_conf_groups{3}{3}=[ind_conf_groups{1}{3}];
ind_conf_groups{3}{4}=[ind_conf_groups{1}{4}];
ind_conf_groups{3}{5}=[ind_conf_groups{1}{5}];

% Acquisition / scanner related confounds
ind_conf_groups{3}{6}=[ind_conf_groups{1}{5},  ind_conf_groups{1}{6}, ...
                       ind_conf_groups{1}{7},  ind_conf_groups{1}{8}, ...
                       ind_conf_groups{1}{9},  ind_conf_groups{1}{10},...
                       ind_conf_groups{1}{11}, ind_conf_groups{1}{12},...
                       ind_conf_groups{1}{13}, ind_conf_groups{1}{14},...
                       ind_conf_groups{1}{15}, ind_conf_groups{1}{16},...
                       ind_conf_groups{1}{17}, ind_conf_groups{1}{18}];
% Motion confounds
ind_conf_groups{3}{7}=[ind_conf_groups{1}{19}];
ind_conf_groups{3}{8}=[ind_conf_groups{1}{20}];
ind_conf_groups{3}{9}=[ind_conf_groups{1}{21}];
ind_conf_groups{3}{10}=[ind_conf_groups{1}{22}];
ind_conf_groups{3}{11}=[ind_conf_groups{1}{23}];
ind_conf_groups{3}{12}=[ind_conf_groups{1}{24}];
ind_conf_groups{3}{13}=[ind_conf_groups{1}{25}];
ind_conf_groups{3}{14}=[ind_conf_groups{1}{26}];
ind_conf_groups{3}{15}=[ind_conf_groups{1}{27}];
ind_conf_groups{3}{16}=[ind_conf_groups{1}{28}];

num_groups(3)=16;
group_Names{3} = {'AGE', 'SEX', 'AGE_SEX', 'HEAD_SIZE', 'SITE', 'ACQ_PARAMS', ...
                  'STRUCT_MOTION', 'DVARS', 'HEAD_MOTION', ...
                  'HEAD_MOTION_ST', 'TABLE_POS', 'EDDY_QC', ...
                  'NON_LIN','CROSSED_TERMS','ACQ_TIME', 'ACQ_DATE',};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Name the different groupings of confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plotNames={'NORMAL', 'REDUCED', 'SPECIAL'};
              
%%%%%%%%%%%%
% CLEANING %
%%%%%%%%%%%%
clear all_PCA;
clear PCs;
clear IDPs;
clear conf_ind;
clear Princ_Components;
clear ESM;
clear HE;
clear AbyB;
clear BbyA;
clear ans;
clear ALL_IDPs;
clear BETAs_all;
clear DVARS;
clear NEWDVARS;
clear NEWDVARS2;
clear Eddy_QC;
clear Eddy_QC2;
clear Eigen_Spectra_Matrix;
clear Horizontal_EigenVectors;
clear HeadMotionST;
clear HeadMotionST2;
clear NET100;
clear NET25;
clear NODEamps100;
clear NODEamps25;
clear Princ_Components;
clear Princ_Components_st;
clear RepT1Values;
clear FlippedSWIValues;
clear Y;
clear AbyB;
clear BbyA;
clear indSite_st;
clear cont;
clear i;
clear max_VE;
clear namF;
clear numDateComponents;
clear previous;
clear complete_PC;
clear birth_date;
clear confAcqTime;
clear confAcqTimeCell;
clear confAcqDateCell;
clear confAge;
clear confAgeLinear;
clear confAgeSex;
clear confBatch;
clear confCoil;
clear confDumb_time;
clear confDVARS;
clear confAcqTime;
clear confEddyQC;
clear confEntropy;
clear confHeadMotion;
clear confHeadSize;
clear confHeadMotionST;
clear confR001;
clear confR010;
clear confR100;
clear confRMAX;
clear confStructHeadMotion;
clear confCMRR;
clear confProtocol;
clear confServicePack;
clear confQuench;
clear confFlippedSWI;
clear confSex;
clear confSite_st;
clear confAcqDate;
clear confX;
clear confR;
clear confTPmedabs;
clear confTablePos;
clear confNewEddy;
clear confScaling;
clear confTE;
clear conf_Non_Temporal;
clear conf_Non_Temporal_Group;
clear confstmp_all;
clear datedelta;
clear day_fraction;
clear denominator;
clear gausskernel;
clear headMotion;
clear headSize;
clear indexX;
clear j;
clear k;
clear namesAcqTime;
clear namesAge;
clear namesAgeSex;
clear namesBatch;
clear namesCoil;
clear namesDVARS;
clear namesNEWDVARS;
clear namesAcqTime;
clear namesEddyQC;
clear namesEntropy;
clear namesHeadMotion;
clear namesHeadSize;
clear namesHeadMotionST;
clear namesR001;
clear namesR010;
clear namesR100;
clear namesStructHeadMotion;
clear namesCMRR;
clear namesProtocol;
clear namesQuench;
clear namesServicePack;
clear namesFlippedSWI;
clear namesSex;
clear namesSite;
clear namesAcqDate;
clear namesTablePos;
clear namesNewEddy;
clear namesScaling;
clear namesTE;
clear namesX;
clear numerator;
clear princ_components_cell;
clear princ_components_cell_st;
clear sex;
clear smoothed_subset_IDPs;
clear subset_IDPs_i_deconf;
clear table;
clear tablePosS;
clear total_princ_components_cell;
clear valueSite;
clear ESM;
clear HE;
clear ALL_IDPs_st;
clear NODEamps25_st;
clear NODEamps100_st;
clear NET25_st;
clear NET100_st;
clear loaded_smoothed_subset_IDPs;
clear loaded_smoothed_subset_IDPs_st;
clear subset_IDPs_i_st;
clear subset_IDPs_i_deconf_st;
clear smoothed_subset_IDPs_st;
clear total_princ_components_cell_st;
clear numTempComponents;
clear DUMB_TIME_data;
clear unsorted_index_sortedTime;
clear new_index_sortedTime;
clear index_sortedDate;
clear index_sortedTime;
clear namesRMAX;
clear ind_IDPs_to_include;
clear ind_IDPs_to_exclude;
clear ind_names_to_include;
clear sortedTime;
clear sortedDate;
clear scan_date;
clear scan_date_cont;
clear confAcqTimeLinear;
clear namesDumb_time;
clear confDumb_timeLinear;
clear confDumb_timeSquared;
clear confSite;
clear dataDir;
clear FMRIB_info;
clear age;
clear ans;
clear confAcqDateLinear;
clear confAge;
clear confScanRamp;
clear confScanColdHead;
clear confScanHeadCoil;
clear confScanMisc;
clear confAge_nonlin;
clear confAgeSex_nonlin;
clear confDVARS_nonlin;
clear confEDDYQC_nonlin;
clear confHEADMOTION_nonlin;
clear confHEADMOTIONST_nonlin;
clear confHEADSIZE_nonlin;
clear confSTRUCTHEADMOTION_nonlin;
clear confTABLEPOS_nonlin;
clear confSCALING_nonlin;
clear confTE_nonlin;
clear namesAcqDateLinear;
clear namesAge;
clear namesAge_nonlin;
clear namesAgeSex_nonlin;
clear namesDVARS_nonlin;
clear namesEDDYQC_nonlin;
clear namesHEADMOTION_nonlin;
clear namesHEADMOTIONST_nonlin;
clear namesHEADSIZE_nonlin;
clear namesSTRUCTHEADMOTION_nonlin;
clear namesTABLEPOS_nonlin;
clear namesSCALING_nonlin;
clear namesTE_nonlin;
clear namesScanRamp;
clear namesScanColdHead;
clear namesScanHeadCoil;
clear namesScanMisc;

my_log(fm, 'After load');

make_dir('TXT_DATA/CORR');
make_dir('HTML/CORR');

for I = 1:length(ind_conf_groups)
    
    labels = group_Names{I};

    for i = 1:length(ind_conf_groups{I})
        labels{i} = strrep(labels{i},'_', ' ');
    end

    mat=[];
    for i=1:length(ind_conf_groups{I})
        my_log(fm, ['Loop: ' num2str(I) ' / ' num2str(i)]);
        for j=i:length(ind_conf_groups{I})
            [mat(i,j),mat(j,i)]=space_spanned(conf(:,ind_conf_groups{I}{i}),...
                                              conf(:,ind_conf_groups{I}{j}));
        end
    end

    dlmwrite(['TXT_DATA/CORR/correlations_matrix_' num2str(I) '.txt'], mat);
    fileName=['TXT_DATA/CORR/names_' num2str(I) '.txt'];
    filePH=fopen(fileName,'w');
    for i=1:length(group_Names{I})
        fprintf(filePH,'%s\n', group_Names{I}{i});
    end
    fclose(filePH);
    
    if I == 4
        fig_size = [0,0,3900,3600];
        font_size = 40;
    else
        fig_size = [0,0,1300,1200];
        font_size = 17;
    end
    f = figure('visible', 'off','PaperOrientation', 'landscape',...
               'Position', fig_size);
    imagesc(log10(mat),[-2,2]);

    colorbar;
    set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');

    set(gca, 'FontSize',font_size);
    set(gca, 'XTick', [1:length(ind_conf_groups{I})], 'XTickLabel', labels);
    set(gca, 'YTick', [1:length(ind_conf_groups{I})], 'YTickLabel', labels);
    set(gca, 'LooseInset', get(gca, 'TightInset'));
    xtickangle(90);
    title({'log_1_0 of % Variance explained by each group of confounds'});

    print('-dpng','-r70',strcat('figs/CORR/correlations_matrix_all_', ...
                                                    plotNames{I},'.png'));

    [s, c] = system(['scripts/script_02_01_ve.py '...
                 'TXT_DATA/CORR/correlations_matrix_' num2str(I) '.txt '...
                 'TXT_DATA/CORR/names_' num2str(I) '.txt '  ...
                 'HTML/CORR/corr_' plotNames{I} '.html ' ...
                 '--apply_log']);                           
end

clear i;
clear j;
clear f;
clear I;
clear new_i;
clear new_j;
clear mat;
clear labels;
clear group_Names_corr;
clear new_group_Names;
clear new_conf_Group;
clear subset_IDPs;
clear ALL_IDPs;
clear IDPs_for_Site;

ind_conf = 1:size(conf,2);

if check_GPU()
    subset_IDPs_i = gather(subset_IDPs_i);
end

save('workspaces/ws_02/IDPs_i.mat', '-v7.3', 'subset_IDPs_i');

clear IDP_i_deconf;
clear IDP_i;

save('workspaces/ws_02/confounds_generated.mat', '-v7.3');
my_log(fm, 'End');
