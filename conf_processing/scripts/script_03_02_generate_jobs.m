%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

f_03_04_s   = 'jobs/jobs_03_04_BA_plots_short.txt';
f_03_04_l   = 'jobs/jobs_03_04_BA_plots_long.txt';
f_03_05_B_s = 'jobs/jobs_03_05_BA_plots_BODY_short.txt';
f_03_05_B_l = 'jobs/jobs_03_05_BA_plots_BODY_long.txt';
f_03_05_C_s = 'jobs/jobs_03_05_BA_plots_COGN_short.txt';
f_03_05_C_l = 'jobs/jobs_03_05_BA_plots_COGN_long.txt';

rm_file(f_03_04_s);
rm_file(f_03_04_l);
rm_file(f_03_05_B_s);
rm_file(f_03_05_B_l);
rm_file(f_03_05_C_s);
rm_file(f_03_05_C_l);

load('workspaces/ws_03/ws_03_subset_IDPs_i.mat');
load('workspaces/ws_03/ws_03_BODY_vars_i.mat');
load('workspaces/ws_03/ws_03_COGN_vars_i.mat');

num_IDPs = size(subset_IDPs_i,2);
num_BODY_vars = size(BODY_vars_i,2);
num_COGN_vars = size(COGN_vars_i,2);

num_IDPs_1 = floor(num_IDPs * 0.33); % Splitting variables to use 2 SGE queues
num_BODY_vars_1 = floor(num_BODY_vars * 0.33); % Splitting variables to use 2 SGE queues
num_COGN_vars_1 = floor(num_COGN_vars * 0.33); % Splitting variables to use 2 SGE queues


% For first section of non-IDPs
for i = 1:num_IDPs_1
    fileID = fopen(f_03_04_s, 'a');
    fprintf(fileID, './scripts/script_03_04_BA_plots.sh %s\n', num2str(i));
    fclose(fileID);    
end

% For second section of non-IDPs
for i = num_IDPs_1+1:num_IDPs
    fileID = fopen(f_03_04_l, 'a');
    fprintf(fileID, './scripts/script_03_04_BA_plots.sh %s\n', num2str(i));
    fclose(fileID);    
end

% For first section of BODY non-IDPs
for i = 1:num_BODY_vars_1
    fileID = fopen(f_03_05_B_s, 'a');
    fprintf(fileID, './scripts/script_03_05_BA_plots_BODY.sh %s\n', num2str(i));
    fclose(fileID);    
end

% For second section of BODY non-IDPs
for i = num_BODY_vars_1+1:num_BODY_vars
    fileID = fopen(f_03_05_B_l, 'a');
    fprintf(fileID, './scripts/script_03_05_BA_plots_BODY.sh %s\n', num2str(i));
    fclose(fileID);    
end

% For first section of COGN non-IDPs
for i = 1:num_COGN_vars_1
    fileID = fopen(f_03_05_C_s, 'a');
    fprintf(fileID, './scripts/script_03_05_BA_plots_COGN.sh %s\n', num2str(i));
    fclose(fileID);    
end

% For second section of COGN non-IDPs
for i = num_COGN_vars_1+1:num_COGN_vars
    fileID = fopen(f_03_05_C_l, 'a');
    fprintf(fileID, './scripts/script_03_05_BA_plots_COGN.sh %s\n', num2str(i));
    fclose(fileID);    
end
clear;

