%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
load('workspaces/ws_02/ve_04.mat');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

make_dir('TXT_DATA/GEN/');

% Iterate over confound grouping (NORMAL, REDUCED, or ALL)
for I = 1:nCONFg

    ve=[];
    for i=1:length(variance_explained_i{I})
        ve=[ve;variance_explained_i{I}{i}'];
    end
    ve=[ve;variance_explained_i_all{I}];
    
    veu=[];
    for i=1:length(variance_explained_i_uniq{I})
        veu=[veu;variance_explained_i_uniq{I}{i}'];
    end
    veu=[veu;variance_explained_i_all{I}];

    group_Names{I}{num_groups(I)+1} = 'ALL';
    

    Rand_ve=[];
    for i=1:length(variance_explained_RandGroups_i{I})
        Rand_ve=[Rand_ve;variance_explained_RandGroups_i{I}{i}'];
    end
    Rand_ve=[Rand_ve;variance_explained_Rand_i{I}{end}'];


    Rand_veu=[];
    for i=1:length(variance_explained_RandGroups_i_uniq{I})
        Rand_veu=[Rand_veu;variance_explained_RandGroups_i_uniq{I}{i}'];
    end
    Rand_veu=[Rand_veu;variance_explained_Rand_i_uniq{I}{end}'];

    
    num_conf_labels={};
    for i=1:length(ind_conf_groups{I})
       num_conf_labels{i}=[num2str(size(conf(:,ind_conf_groups{I}{i}),2))];
    end

    num_conf_labels{i+1} = [num2str(size(conf,2))];

    
    final_num_conf_labels = {};
    
    for i=1:length(num_conf_labels)
        final_num_conf_labels = [final_num_conf_labels, ...
                                num_conf_labels{i}, num_conf_labels{i}];
    end  
    
    final_ve  = [];
    final_veu = [];
    final_group_names = {};
    
    for i=1:size(Rand_veu,1)
        final_ve  = [final_ve;  ve(i,:) ; Rand_ve(i,:)];
        final_veu = [final_veu; veu(i,:); Rand_veu(i,:)];
        final_group_names = [final_group_names, ...
        strrep(group_Names{I}{i},'_', ' '), ['RAND ' num_conf_labels{i}]];
    end
    
    confGroupsFileName = ['TXT_DATA/GEN/conf_groups_' plotNames{I} '.txt'];
    
    fileID=fopen(confGroupsFileName,'w');

    for i=1:length(num_conf_labels)
        fprintf(fileID,'%s %s\n',group_Names{I}{i}, ...
                                 num2str(num_conf_labels{i}));
    end

    fclose(fileID);
    
    x = {ve,veu};
    y = {Rand_ve,Rand_veu};
    z = {final_ve,final_veu};
    
    lab = {'Variance Explained', 'Unique Variance Explained'};

    plots_dir=strcat('figs/VE_',plotNames{I},'/');
    rm_make_dir(plots_dir)
    txt_dir=['TXT_DATA/VE_' plotNames{I} '/'];
    make_dir(txt_dir);
    html_dir=['HTML/VE_' plotNames{I} '/'];
    make_dir(html_dir);
    
    
    % Iterate over ve (variance explained) 
    %  and veu (unique variance explained)
    for i=1:2
        % Iterate over IDP grouping (ALL or independent)
        for IDP_set=1:length(num_IDP_groups)
            % Iterate over IDP groups in each IDP grouping
            for j=1:num_IDP_groups{IDP_set}

                name_IDP_group = strrep(ind_IDP_groups{IDP_set}(j).name,...
                                                              '.txt', '');
                IDP_inds = ind_IDP_groups{IDP_set}(j).ind;                

                nameOut =strcat(txt_dir, name_IDP_group, '_', ...
                                strrep(lab{i},' ','_'),'.txt');
                nameRand=strcat(txt_dir, '/RAND_', name_IDP_group, '_',...
                                strrep(lab{i},' ','_'),'.txt');
                dlmwrite(nameOut,  x{i}(:,IDP_inds), 'delimiter', ' ');
                dlmwrite(nameRand, y{i}(:,IDP_inds), 'delimiter', ' ');
                
                IDPNamesFilename = ['TXT_DATA/GEN/names_' name_IDP_group ...
                                                                   '.txt'];
                fileID=fopen(IDPNamesFilename,'w');

                for k=IDP_inds
                    fprintf(fileID,'%s\n',IDPnames{k});
                end

                fclose(fileID);


                f = figure('visible', 'off', 'PaperOrientation','landscape',...
                          'Units','points','Position', [0,0,2000 750]);

                % Code taken from github.com/bastibe/Violinplot-Matlab
                splitViolinplot(log10(z{i}(:,IDP_inds)'),...
                      final_group_names, 'BoxWidth', 0.06, 'BoxColor', ...
                      [0.5 0.5 0.5], 'EdgeColor', [0.2 0.2 0.2]);
                ti = title(strcat(strrep(name_IDP_group,{'_'},{' '}),...
                           {' '}, lab{i}));
                coord = get(ti, 'Position');
                set(ti,'Position', [coord(1), 2.3, 0]);
                    
                ax1=gca;
                set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', ...
                                                                'portrait');
                set(gca, 'FontSize', 17, 'yscale', 'linear');
                set(gca, 'XGrid', 'off');
                set(gca, 'YGrid', 'on');
                xtickangle(90);
                ylabel('Log_1_0 % of variance explained');
                
                ylim(log10([10^(-4),100]));
                ax2=axes('Position', get(ax1, 'Position'), 'Color', 'None');

                set(ax2,'XAxisLocation','Top');
                set(ax2,'XLim', get(ax1,'XLim'));
                set(ax2,'XTick', get(ax1,'XTick'));
                set(ax2, 'FontSize',12);
                set(ax2,'XTickLabel', final_num_conf_labels,'YTickLabel', {[]});
                set(gca, 'LooseInset', get(gca, 'TightInset'));
                set(ax2, 'LooseInset', get(ax2, 'TightInset'));
                print('-dpng','-r70',strcat(plots_dir, name_IDP_group, ...
                                     '_', strrep(lab{i},' ','_'),'.png'));
              
                htmlFilename = [html_dir name_IDP_group '_' ...
                                strrep(lab{i}, ' ','_') '.html'] ;
                            
                [s, c] = system(['scripts/script_02_07_ve.py ' nameOut ...
                                 ' ' nameRand ' ' confGroupsFileName ...
                                 ' ' IDPNamesFilename ' ' htmlFilename ...
                                 ' --apply_log']);
            end
        end
    end
end


% Get a list of the mean UVE per group, excluding 
% NON-LIN, CROSSED TERMS, ACQ_DATE and ACQ_TIME
list_UVEs = [];
confGroupsFileName = ['tables/UVE_groups.txt'];
fileID=fopen(confGroupsFileName,'w');

for i=1:length(ind_conf_groups{1})-4
    num_UVE = nanmean(log10(variance_explained_i_uniq{1}{i}));
    fprintf(fileID, [num2str(i), ' | ', group_Names{1}{i}, ' | ', ...
             num2str(num_UVE),'\n']);
    list_UVEs = [list_UVEs; num_UVE];
end

my_log(fm, 'End');
