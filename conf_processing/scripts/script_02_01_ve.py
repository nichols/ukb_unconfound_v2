#!/bin/env python
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import plotly
import random
import matplotlib
import numpy as np
import plotly.plotly as py
import sys,argparse,os.path
import plotly.graph_objs as go

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():

    parser = MyParser(description='Correlation matrix generation tool')
    parser.add_argument("dataFile", help='Data to generate correlation matrix')
    parser.add_argument("namesFile", help='Names for the categories')
    parser.add_argument("outputFile", nargs='?', default='./corr_matrix', \
                                        help='Output file')
    parser.add_argument('--apply_log', default=False, action='store_true', \
                        help='Apply log_10 to the data')

    argsa = parser.parse_args()

    dataFile   = argsa.dataFile
    namesFile  = argsa.namesFile
    outputFile = argsa.outputFile
    apply_log  = argsa.apply_log

    if not outputFile.endswith('.html'):
        outputFile = outputFile + '.html'

    z = np.loadtxt(dataFile, delimiter=",")

    z[np.where(z<0.01)] = 0.01

    with open(namesFile, 'r') as f:
        names = f.readlines()
    x = [i.strip() for i in names]
    y = [i.strip() for i in names]

    colorbar_title = ""
    if apply_log:
        z_no_log = z.tolist()
        z = np.log10(z)
        colorbar_title = 'Log10'

    labels=[]

    for i in range(len(x)):
        new_list = []
        for j in range(len(y)):
            if z_no_log[len(x)-i-1][j] == 0.01:
                val = "Less than 0.01%"
            else:
                val = str(z_no_log[len(x)-i-1][j]) + "%"

            if (len(x)-i-1) == j:
                new_list.append(x[j])
            else:
                new_list.append(val + " of variance of " + x[len(x)-i-1] + \
                            " explained by " + y[j])
        labels.append(new_list)

    #Needed by plotly
    z = z.tolist()

    y.reverse()
    z.reverse()

    plotly.offline.plot(
        { "data": [ go.Heatmap(x = x, y = y, z = z, colorscale = 'Viridis', \
                               colorbar=dict(title = colorbar_title,\
                               ticks = 'outside'),
                               hoverinfo='text', text=labels)],
          "layout": go.Layout(title = "% of variance explained by group of confounda",
                              xaxis = dict(ticks='', tickangle=270, nticks=len(x)),
                              yaxis = dict(ticks=''), \
                              margin=go.layout.Margin(l=150,r=50,b=150,t=50,pad=10),\
                              hovermode = 'closest')},
          auto_open = False, filename = outputFile, include_mathjax='cdn')

if __name__ == "__main__":
    main()
