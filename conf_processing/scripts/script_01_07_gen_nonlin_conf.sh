#!/bin/bash

cat tables/*UVE_nonlin.txt | awk '{print $1}' | sort | uniq > tables/list_nonlin.txt
 
for elem in `cat tables/list_nonlin.txt | grep squared_inormal` ; do 
    red=${elem%_squared_inormal};
    echo ${red}_inormal>> tables/list_nonlin.txt
done

cat tables/list_nonlin.txt | sort | uniq > tables/list_nonlin.txt2
mv tables/list_nonlin.txt2 tables/list_nonlin.txt
