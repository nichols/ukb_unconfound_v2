#!/bin/env python
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import sys
import glob
import argparse
import datetime
import numpy as np
import plotly
import plotly.graph_objs as go

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def FDR(p, alpha=0.05):

    p = p.flatten()

    p = p[~np.isnan(p)]
    p = np.sort(p)
    V = p.shape[0]
    I = np.arange(1, V + 1)

    cVID = 1
    cVN = np.sum(1 / I)

    valID = ((I / V) * alpha) / cVID
    valN =  ((I / V) * alpha) / cVN

    pIDn = np.where(p <= valID)[0]
    pNn  = np.where(p <= valN)[0]

    pID = p[np.max(pIDn)] if len(pIDn) != 0 else 0
    pN  = p[np.max(pNn)]  if len(pNn) != 0 else 0

    return [pID, pN]

def main():

    parser = MyParser(description='Manhattan Plot generation tool')
    parser.add_argument("R_data", \
                      help='Data for the Rho coefficient of the correlations')
    parser.add_argument("P_data", \
                          help='Data for the P values of the correlations')
    parser.add_argument("N_data", help='Data for the N of the correlations')
    parser.add_argument("IDPs_labels", help='Labels for the IDPs')
    parser.add_argument("nIDPs_labels", help='Labels for the nIDPs')
    parser.add_argument("IDP_groups_Dir", default='./', \
                                     help='Directory with the IDP groups')
    parser.add_argument("nIDP_groups_Dir", default='./', \
                                     help='Directory with the non IDP groups')
    parser.add_argument("inputDir", nargs='?', default='./', \
                                     help='Input Directory')
    parser.add_argument("outputDir", nargs='?', default='./', \
                                     help='Output Directory')
    parser.add_argument("title", nargs='?', default='./', \
                                     help='-log10(P values)')
    parser.add_argument("alpha", nargs='?', type=float, default=0.05, \
                                     help='Alpha for the significance')

    argsa = parser.parse_args()

    # Make this an argument?
    P_min_value = 3

    R_data          = argsa.R_data
    P_data          = argsa.P_data
    N_data          = argsa.N_data
    IDPs_labels     = argsa.IDPs_labels
    nIDPs_labels    = argsa.nIDPs_labels
    IDP_groups_Dir  = argsa.IDP_groups_Dir
    nIDP_groups_Dir = argsa.nIDP_groups_Dir
    inputDir        = argsa.inputDir
    outputDir       = argsa.outputDir
    alpha           = argsa.alpha
    plot_title      = argsa.title

    print('Before loading data - ' + str(datetime.datetime.now()))

    R =  np.loadtxt(inputDir + '/' + R_data + '.txt')
    P =  np.loadtxt(inputDir + '/' + P_data + '.txt')
    N =  np.loadtxt(inputDir + '/' + N_data + '.txt')
    f = open(inputDir + '/' + IDPs_labels + '.txt', 'r')
    IDPs_labels = [x.strip() for x in f.readlines()]
    f.close()
    f = open(inputDir + '/' + nIDPs_labels + '.txt', 'r')
    nIDPs_labels = [x.strip() for x in f.readlines()]
    f.close()

    print('After loading data. Before loading groups - ' + \
          str(datetime.datetime.now()))

    num_nIDPs = P.shape[1]

    IDP_groups = glob.glob(IDP_groups_Dir + '/*.txt')
    IDP_groups = [x.replace(IDP_groups_Dir, '') for x in IDP_groups]
    IDP_groups = [x.replace('.txt', '').replace('/', '') for x in IDP_groups]
    IDP_groups.sort()

    IDP_inds = {}
    for IDP_group in IDP_groups:
        vals = np.loadtxt(IDP_groups_Dir + '/' + IDP_group + '.txt')
        # Ugly, but I need to do this because np.loadtxt does
        # not return an array if a file has just one element
        if vals.size == 1:
            vals = [vals]
        IDP_inds[IDP_group] = [int(x) - 1 for x in vals]

    nIDP_groups = glob.glob(nIDP_groups_Dir + '/*.txt')
    nIDP_groups = [x.replace(nIDP_groups_Dir, '') for x in nIDP_groups]
    nIDP_groups = [x.replace('.txt', '').replace('/', '') for x in nIDP_groups]
    nIDP_groups.sort()

    print('After loading groups. Before reading groups - ' +
          str(datetime.datetime.now()))

    nIDP_inds = {}
    for nIDP_group in nIDP_groups:
        vals = np.loadtxt(nIDP_groups_Dir + '/' + nIDP_group + '.txt')
        # Ugly, but I need to do this because np.loadtxt does
        # not return an array if a file has just one element
        if vals.size == 1:
            vals = [vals]
        nIDP_inds[nIDP_group] = [int(x) - 1 for x in vals]

    IDP_inds = {}
    for IDP_group in IDP_groups:
        vals = np.loadtxt(IDP_groups_Dir + '/' + IDP_group + '.txt')
        # Ugly, but I need to do this because np.loadtxt does
        # not return an array if a file has just one element
        if vals.size == 1:
            vals = [vals]
        IDP_inds[IDP_group] = [int(x) - 1 for x in vals]

    print('After creating labels. Before finding max - ' +
          str(datetime.datetime.now()))

    max_indices = {}
    for IDP_group in IDP_groups:
        vals = P[IDP_inds[IDP_group]]
        ind = np.argmax(vals, 0)
        max_indices[IDP_group] = [IDP_inds[IDP_group][x] for x in ind]


    print('After finding max. Before creating data to plot - ' +
         str(datetime.datetime.now()))

    FDR_V  = -np.log10(FDR(np.power(10, -P.flatten()), alpha)[0])
    BONF_V = -np.log10(alpha / (len(P.flatten())))

    # Slightly extending the thresholds
    maxY = np.nanmax([np.nanmax(P.flatten()), FDR_V, BONF_V]) * 1.03
    minY = np.nanmin([P_min_value, FDR_V, BONF_V]) * 0.97
    minX = -1
    maxX = num_nIDPs + 1

    finalX = {}
    finalY = {}
    finalT = {}
    for IDP_group in IDP_groups:
        finalX[IDP_group] = []
        finalY[IDP_group] = []
        finalT[IDP_group] = []
        for i in range(num_nIDPs):
            if P[max_indices[IDP_group][i]][i] > minY:
                IDP_label = IDPs_labels[max_indices[IDP_group][i]]
                nIDP_label = nIDPs_labels[i]
                rho = R[max_indices[IDP_group][i]][i]
                P_value = P[max_indices[IDP_group][i]][i]
                N_NaN = N[max_indices[IDP_group][i]][i]
                finalX[IDP_group].append(i)
                finalY[IDP_group].append(P_value)
                finalT[IDP_group].append(['IDP: ' + IDP_label + '<BR>nIDP:' +
                                         nIDP_label + '<BR>Rho: ' + str(rho) +
                                         '<BR>-log10 P: ' + str(P_value) +
                                         '<BR>N: ' + str(N_NaN)])

    print('After creating data to plot. Before plotting - ' +
           str(datetime.datetime.now()))

    # Calculating needed values for the plot



    # Creating vertical an horizontal dashed lines
    shapes = list()
    for nIDP_ind in nIDP_inds:
        shapes.append({
                        'type': 'line', 'line' : dict(width=1, dash="dot"),
                        'xref': 'x', 'yref': 'y', 'y0': 0, 'y1': maxY,
                        'x0': nIDP_inds[nIDP_ind][-1],
                        'x1': nIDP_inds[nIDP_ind][-1]}
                      )

    # Creating the values for the ticks in the X axis
    x_ticks = []
    for nIDP_group in nIDP_groups:
        x_ticks.append(int(nIDP_inds[nIDP_group][0] +
                       (len(nIDP_inds[nIDP_group])/2)))

    # Creating the data to plot
    data_list = []
    for IDP_group in IDP_groups:
        data_list.append(go.Scattergl(x=finalX[IDP_group],
                                      y=finalY[IDP_group],
                                      text=finalT[IDP_group],
                                      mode='markers', name=IDP_group,
                                      marker=dict(
                                        line=dict(width=0),
                                        color=IDP_groups.index(IDP_group),
                                        size=4)
                                      )
                         )
    data_list.append(go.Scattergl(x=[minX, maxX], y=[BONF_V, BONF_V],
                                  mode='lines', name='BONF',
                                  line=dict(width=2, color='black')
                                  )
                     )
    data_list.append(go.Scattergl(x=[minX, maxX], y=[FDR_V, FDR_V],
                                  mode='lines', name='FDR',
                                  line=dict(width=2, dash="dash",
                                             color='black')
                                  )
                     )

    # Actual plotting
    plotly.offline.plot({
        "data": data_list,
        "layout": go.Layout(title=go.layout.Title(
                            text=plot_title.replace('_', ' '),
                            y=0.97),
                            xaxis=go.layout.XAxis(
                                title=go.layout.xaxis.Title(
                                    text='non-Imaging variables',
                                    font=dict(size=20)
                                ),
                                ticks='outside', tickmode='array',
                                tickvals=x_ticks,
                                ticktext=[x.replace('_', ' ')
                                                for x in nIDP_groups],
                                tickangle=270, automargin=True,
                                range=[minX, maxX]
                            ),
                            yaxis=go.layout.YAxis(
                                title=go.layout.yaxis.Title(
                                    text='-log10(P)', font=dict(size=20)
                                ),
                                # The range expands sligtly the real margins.
                                range=[np.log10(minY), np.log10(maxY)],
                                ticks='outside'
                            ),
                            hovermode='closest',
                            shapes=shapes,
                            yaxis_type='log',
                    )
                },
                auto_open=False,
                filename=outputDir + '/MNHT_' + plot_title.upper() + '.html')

    print('After plotting. End - ' + str(datetime.datetime.now()))

if __name__ == "__main__":
    main()
