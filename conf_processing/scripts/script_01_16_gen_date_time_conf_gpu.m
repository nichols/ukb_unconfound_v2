%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
load('workspaces/ws_01/ws_01_15.mat');

finalFile = 'workspaces/ws_01/ws_01_16.mat';
rm_file(finalFile);

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

namesDir = '../data/NAMES_confounds/'; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Joining together all the confounds so far and generating names %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
conf_Non_Temporal= [confAge confSex confAgeSex confHeadSize confSite ...
           confBatch confCMRR confProtocol confServicePack confScanRamp ...
           confScanColdHead confScanHeadCoil confScanMisc ...
           confFlippedSWI confFST2 confNewEddy confScaling confTE ...
           confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
           confTablePos confEddyQC conf_nonlin conf_ct];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC,conf_nonlin,conf_ct};      

%Deconfounding IDPs with every confound model
if check_GPU()
    conf_Non_Temporal = gather(conf_Non_Temporal);
    subset_IDPs_i = gather(subset_IDPs_i);
end

subset_IDPs_i_deconf = nets_unconfound2(subset_IDPs_i, conf_Non_Temporal);

my_log(fm, 'After unconfound for ACQ Time');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sort most important variables according to time of the day %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
confAcqTimeLinear = nets_normalise(day_fraction); 
confAcqTimeLinear(isnan(confAcqTimeLinear)) = 0;

[sortedTime, index_sortedTime] = sort(confAcqTimeLinear);

subset_IDPs_i_st        = subset_IDPs_i(index_sortedTime,:);
subset_IDPs_i_deconf_st = subset_IDPs_i_deconf(index_sortedTime,:);
siteDATA_st             = siteDATA(index_sortedTime,:);

%Generating inverse indices
unsorted_index_sortedTime=1:length(index_sortedTime);
new_index_sortedTime(index_sortedTime)=unsorted_index_sortedTime;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the indices of each subject per site %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
indSite_st=cell(length(siteValues),1);
for j = 1:length(siteValues)
    valueSite=siteValues(j);
    indSite_st{j}=find(all(siteDATA_st==siteValues(j),2));
end

clear valueSite j;

%%%%%%%%%%%%%%%%%%%
% Simple cleaning %
%%%%%%%%%%%%%%%%%%%
clear f;
clear i;
clear j;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finding ACQ TIME confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if check_GPU()
    subset_IDPs_i_deconf_st = gpuArray(subset_IDPs_i_deconf_st);
    subset_IDPs_i_st        = gpuArray(subset_IDPs_i_st);       
end 
loaded_smoothed_subset_IDPs_st = cell(length(indSite_st),1);
confAcqTimeCell                = cell(length(indSite_st),1);
total_princ_components_cell_st = cell(length(indSite_st),1);
total_ESM_st                   = cell(length(indSite_st),1);
princ_components_cell_st       = cell(length(indSite_st),1);

numSites=length(indSite_st);
sigma = 0.1;
 
for i = 1:numSites
     IDPs_for_Site = subset_IDPs_i_deconf_st(indSite_st{i},:);
     %Iterate through subjects in site i
     for j = 1:length(indSite_st{i})
         if mod(j, 100) == 0
            my_log('', [' ' num2str(i) ' / ' num2str(j)]);
         end
 
         smoothed_subset_IDPs_st=[];
         if check_GPU()
            timedelta = gpuArray(sortedTime(indSite_st{i}(j)) - ...
                                 sortedTime(indSite_st{i}));
         else
            timedelta = sortedTime(indSite_st{i}(j)) - ...
                        sortedTime(indSite_st{i});
         end
         gausskernel = exp( -0.5 * (timedelta / sigma ).^2 );
 
         numerator   = nansum(IDPs_for_Site .* gausskernel);
         denominator = nansum(~isnan(IDPs_for_Site) .*gausskernel);
         smoothed_subset_IDPs_st = numerator ./ denominator;
         if check_GPU()
            loaded_smoothed_subset_IDPs_st{i}(j,:) = gather(smoothed_subset_IDPs_st);
         else
            loaded_smoothed_subset_IDPs_st{i}(j,:) = smoothed_subset_IDPs_st;
         end
         clear timedelta smoothed_subset_IDPs_st;
     end
    
     if check_GPU()
         smoothed_IDPs_st = gpuArray(loaded_smoothed_subset_IDPs_st{i});
     else
         smoothed_IDPs_st = loaded_smoothed_subset_IDPs_st{i};
     end
         
     [Princ_Components_st,ESM,HE]=nets_svds(smoothed_IDPs_st, 0);
     total_princ_components_cell_st{i}=Princ_Components_st;
     total_ESM_st{i} = ESM;
end

my_log(fm, 'After PCA ACQ Time');

% Estimating the number of temporal components by choosing a number
% that explains at least 99% of the variance in she smoothed IDPs.
numTempComponents_st=[];
for j = 1:length(indSite_st)

    max_VE=0;
    numTempComponents_st(j)=1;
    if check_GPU()
        IDPs_st = gpuArray(loaded_smoothed_subset_IDPs_st{j});
    else
        IDPs_st = loaded_smoothed_subset_IDPs_st{j};
    end
    IDP_ind=~isnan(IDPs_st);
    IDP_ind=all(IDP_ind')';

    while(max_VE < 99)
        my_log(fm,[num2str(j) ' / ' num2str(numTempComponents_st(j))  ' / ' ...
                  num2str(max_VE)]);

        if check_GPU()
            PCs_st = gpuArray(total_princ_components_cell_st{j}(:,...
                                                1:numTempComponents_st(j)));
        else
            PCs_st = total_princ_components_cell_st{j}(:,...
                                                1:numTempComponents_st(j));
        end

        [AbyB,BbyA] = space_spanned(PCs_st(IDP_ind,:),IDPs_st(IDP_ind,:));

        max_VE = BbyA;
        if max_VE < 99
            numTempComponents_st(j)=numTempComponents_st(j)+1;
        end
        clear PCs;
    end
    clear IDPs;
end

my_log(fm, 'After estimating the number of PCs');

for j = 1:length(indSite_st)
    princ_components_cell_st{j}=total_princ_components_cell_st{j}(:,...
                                                  1:numTempComponents_st(j));

    confAcqTimeCell{j} = nets_normalise(princ_components_cell_st{j});  
    confAcqTimeCell{j}(isnan(confAcqTimeCell{j}))=0; 
end

cont=1;
confAcqTime=zeros(size(subset_IDPs_i_st,1),sum(numTempComponents_st));
namesAcqTime={};

for j = 1:length(indSite_st)
    for k = 1:numTempComponents_st(j)
        if check_GPU()
            confAcqTime(indSite_st{j},cont)=gather(confAcqTimeCell{j}(:,k));
        else
            confAcqTime(indSite_st{j},cont)=confAcqTimeCell{j}(:,k);
        end
        namesAcqTime{cont,1}=strcat('ACQT_Site_', num2str(j), '__', ...
                                     sprintf('%02d',k));
        cont=cont+1;
    end
end

%Reversing confAcqTime to the original sorting.
for i=1:size(confAcqTime,2)
    confAcqTime(:,i)=confAcqTime(new_index_sortedTime,i);
end

if check_GPU()
    subset_IDPs_i_deconf_st = gather(subset_IDPs_i_deconf_st);
    subset_IDPs_i_st        = gather(subset_IDPs_i_st);       
end 

clear PCs;
clear IDP_ind;
clear HE;
clear max_VE;
clear AbyB;
clear BbyA;
clear numerator;
clear denominator;
clear gausskernel;
clear IDPs_for_Site;
clear ESM_st;
clear IDPs_st;
clear PCs_st;
clear Princ_Components_st;
clear indSite_st;
clear loaded_smoothed_subset_IDPs_st;
clear princ_components_cell_st;
clear siteDATA_st;
clear smoothed_IDPs_st;
clear smoothed_subset_IDPs_st;
clear subset_IDPs_i_deconf_st;
clear subset_IDPs_i_st;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Joining together all the confounds so far %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC conf_nonlin conf_ct confAcqTime];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC,conf_nonlin,conf_ct,confAcqTime}; 

%Deconfounding IDPs with every confound model
if check_GPU()
    conf_Non_Temporal = gather(conf_Non_Temporal);
    subset_IDPs_i = gather(subset_IDPs_i);
end

subset_IDPs_i_deconf = nets_unconfound2(subset_IDPs_i,conf_Non_Temporal);

my_log(fm, 'After Temp defonfound');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finding ACQ DATE confounds %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loaded_smoothed_subset_IDPs = cell(length(indSite),1);
confAcqDateCell             = cell(length(indSite),1);
total_princ_components_cell = cell(length(indSite),1);
total_ESM                   = cell(length(indSite),1);
princ_components_cell       = cell(length(indSite),1);
 
numSites=length(indSite);
sigma = 0.1;
  
for i = 1:numSites
    IDPs_for_Site = subset_IDPs_i_deconf(indSite{i},:);
    for j = 1:length(indSite{i})
        if mod(j, 100) == 0
            my_log('', [' ' num2str(i) ' / ' num2str(j)]);
        end
 
        smoothed_subset_IDPs=[];
        if check_GPU()
            timedelta = gpuArray(sortedDate(indSite{i}(j)) - ...
                                 sortedDate(indSite{i})); 
        else
            timedelta = sortedDate(indSite{i}(j)) - ...
                        sortedDate(indSite{i}); 
        end
        gausskernel = exp( -0.5 * (timedelta / sigma ).^2 );
 
        numerator   = nansum(IDPs_for_Site .* gausskernel);
        denominator = nansum(~isnan(IDPs_for_Site) .*gausskernel);
        smoothed_subset_IDPs = numerator ./ denominator;
         
        if check_GPU()
            loaded_smoothed_subset_IDPs{i}(j,:) = gather(smoothed_subset_IDPs);
        else
            loaded_smoothed_subset_IDPs{i}(j,:) = smoothed_subset_IDPs;
        end
        clear timedelta smoothed_subset_IDPs;
    end
 
    if check_GPU()
        smoothed_IDPs = gpuArray(loaded_smoothed_subset_IDPs{i});
    else
        smoothed_IDPs = loaded_smoothed_subset_IDPs{i};
    end
     
    [Princ_Components,ESM,HE] = nets_svds(smoothed_IDPs,0);
    total_princ_components_cell{i} = Princ_Components;
    total_ESM{i} = ESM;
end
 
my_log(fm, 'After PCA ACQ Date');

numDateComponents=[];
for j = 1:length(indSite)

    max_VE=0;
    numDateComponents(j)=1;

    if check_GPU()
        IDPs=gpuArray(loaded_smoothed_subset_IDPs{j});
    else
        IDPs=loaded_smoothed_subset_IDPs{j};
    end
    IDP_ind=~isnan(IDPs);
    IDP_ind=all(IDP_ind')';

    while(max_VE < 99)
        my_log(fm, [num2str(j) ' / ' num2str(numDateComponents(j))  ...
                   ' / ' num2str(max_VE)]);
        if check_GPU()
            PCs = gpuArray(total_princ_components_cell{j}(:,1:numDateComponents(j)));
        else
            PCs = total_princ_components_cell{j}(:,1:numDateComponents(j));
        end
     
        [AbyB,BbyA] = space_spanned(PCs(IDP_ind,:),IDPs(IDP_ind,:));

        max_VE = BbyA;
        if max_VE < 99
            numDateComponents(j)=numDateComponents(j)+1;
        end
        clear PCS;
    end
    clear IDPs;
end

my_log(fm, 'After estimating number of PCs');

for j = 1:length(indSite)
    princ_components_cell{j}=total_princ_components_cell{j}(:,...
                                                1:numDateComponents(j));

    confAcqDateCell{j} = nets_normalise(princ_components_cell{j});  
    confAcqDateCell{j}(isnan(confAcqDateCell{j}))=0; 
end

cont=1;
confAcqDate=zeros(size(subset_IDPs_i,1),sum(numDateComponents));
namesAcqDate={};

for j = 1:length(indSite)
    for k = 1:numDateComponents(j)
        if check_GPU()
            confAcqDate(indSite{j},cont)=gather(confAcqDateCell{j}(:,k));
        else
            confAcqDate(indSite{j},cont)=confAcqDateCell{j}(:,k);
        end
        namesAcqDate{cont,1}=strcat('DATE_Site_', num2str(j), '__', ...
                                    sprintf('%02d',k));
        cont=cont+1;
    end
end

clear PCs;
clear IDPs;
clear IDP_ind;
clear HE;
clear ESM;
clear max_VE;
clear AbyB;
clear BbyA;
clear numerator;
clear denominator;
clear gausskernel;
clear IDPs_for_Site;
clear smoothed_IDPs;
clear Princ_Components;

clear loaded_smoothed_subset_IDPs;
clear princ_components_cell;
clear siteDATA;
clear smoothed_IDPs;
clear smoothed_subset_IDPs;
clear subset_IDPs_i_deconf;
clear numTempComponents_st;
clear total_ESM_st;
clear total_princ_components_cell_st;
clear numTempComponents;
clear total_ESM;
clear total_princ_components_cell;

save(finalFile, '-v7.3');

rm_make_dir('figs/ACQT_PCA/');
rm_make_dir('figs/ACQD_PCA/');

for i = 1:length(indSite)
    ind_confs = [];
    
    for j = 1:size(confAcqTime,2)
        if strfind(namesAcqTime{j}, strcat('Site_', num2str(i)))
            ind_confs = [ind_confs; j];
        end
    end
    
    for j = ind_confs'
        title_prep = strrep(namesAcqTime{j},'_', ' ');
        f = figure('visible','off','PaperOrientation', 'landscape','Units', ...
               'points','Position', [0,0,800,600]);
        dscatter(day_fraction(indSite{i}),confAcqTime(indSite{i},j));
        set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
        set(gca, 'FontSize',20);
        xlabel('Time of acquisition in [0, 1] range.', 'FontSize', 18);
        title(['Temporal component: ' title_prep])
        print('-dpng','-r70',['figs/ACQT_PCA/' namesAcqTime{j} '.png']);
    end
end

for i = 1:length(indSite)
    ind_confs = [];
    
    for j = 1:size(confAcqDate,2)
        if strfind(namesAcqDate{j}, strcat('Site_', num2str(i)))
            ind_confs = [ind_confs; j];
        end
    end
    
    for j = ind_confs'
        title_prep = strrep(namesAcqDate{j},'_', ' ');
        f = figure('visible','off','PaperOrientation', 'landscape','Units', ...
               'points','Position', [0,0,800,600]);
        dscatter(sortedDate(indSite{i}),confAcqDate(indSite{i},j));
        set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
        set(gca, 'FontSize',20);
        xlabel('Acquisition date', 'FontSize', 18);
        title(['Temporal component: ' title_prep])
        print('-dpng','-r70',['figs/ACQD_PCA/' namesAcqDate{j} '.png']);
    end
end

my_log(fm, 'End');
