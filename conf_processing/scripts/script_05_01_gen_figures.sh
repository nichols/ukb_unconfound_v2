#!/bin/bash
#
#   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
#   Copyright 2017 University of Oxford
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

rm -f figs/*.png;
rm -f figs/BA_*/c_*;
rm -f figs/ACQ*_PCA/c_*
rm -f figs/NAT/c_*

rm   -rf figs/Figs/
rm   -rf figs/SM/
mkdir -p figs/Figs/
mkdir -p figs/SM/

# Create Fig 1
cp figs/CORR/correlations_matrix_all_NORMAL.png figs/Figs/Fig_01.png
echo "Fig 1 created - `date`"

# Create Fig 2
convert figs/NONLIN/mean_UVE_across_IDPs.png \
        figs/NONLIN/max_UVE_across_IDPs.png \
        figs/NONLIN/manhattan1.png -append \
        -shave 2x2 -bordercolor black -border 2 figs/Figs/Fig_02.png
echo "Fig 2 created - `date`"

# Create Fig 3
convert figs/CT/mean_UVE_across_IDPs.png \
        figs/CT/max_UVE_across_IDPs.png \
        figs/CT/manhattan1.png -append \
        -shave 2x2 -bordercolor black -border 2 figs/Figs/Fig_03.png
echo "Fig 3 created - `date`"

# Create Fig 4
convert figs/VE_NORMAL/ALL_IDPs_Unique_Variance_Explained.png \
        figs/VE_REDUCED/ALL_IDPs_Unique_Variance_Explained.png \
        -append -crop 1550x2000+190+0 \
        -shave 2x2 -bordercolor black -border 2 figs/Figs/Fig_04.png
echo "Fig 4 created - `date`"

# Cropping and adding a border on all BA plots
for elem in figs/BA_*/* ; do
    dire=`dirname $elem`;
    nam=`basename $elem`;
    convert $elem -crop 870x690+35+0 -shave 2x2 \
            -bordercolor black -border 2 $dire/c_$nam;
done

# Create Fig 5 
convert figs/BA_COGN/c_BA_AGE_COGN.png \
        figs/BA_BODY/c_BA_AGE_BODY.png \
        figs/BA_COGN/c_BA_CROSSED_TERMS_COGN.png \
        -append figs/tmp_1.png
convert figs/BA_COGN/c_BA_HEAD_SIZE_COGN.png \
        figs/BA_BODY/c_BA_HEAD_SIZE_BODY.png \
        figs/BA_BODY/c_BA_SERVICE_PACK_BODY.png \
        -append figs/tmp_2.png
convert figs/BA_COGN/c_BA_TABLE_POS_COGN.png \
        figs/BA_BODY/c_BA_TABLE_POS_BODY.png \
        figs/BA_BODY/c_BA_DVARS_BODY.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/Figs/Fig_05.png
echo "Fig 5 created - `date`"

# Cropping and adding a border on all NAT figures
for elem in figs/NAT/* ; do
    dire=`dirname $elem`;
    nam=`basename $elem`;
    convert $elem -crop 870x690+35+0 -shave 2x2 \
            -bordercolor black -border 2 $dire/c_$nam;
done

# Create Fig 6
convert figs/NAT/c_1_Age_Site_1.png \
        figs/NAT/c_217_TablePos_COG_X_Site_1.png \
        figs/NAT/c_219_TablePos_COG_Z_Site_1.png \
        figs/NAT/c_73_StructHeadMotion_Site_1.png \
        -append figs/tmp_1.png
convert figs/NAT/c_10_HeadSize_Site_1.png \
        figs/NAT/c_218_TablePos_COG_Y_Site_1.png \
        figs/NAT/c_220_TablePos_Table_Site_1.png \
        figs/NAT/c_136_HeadMotion_mean_tfMRI_abs_Site_1.png \
        -append figs/tmp_2.png
convert figs/tmp_1.png figs/tmp_2.png +append figs/Figs/Fig_06.png
echo "Fig 6 created - `date`"


# Create Fig 7
convert figs/VE_GLOBAL/ALL_IDPs_Variance_Explained.png \
        -crop 1670x2000+210+0 figs/tmp_1.png
convert figs/tmp_1.png figs/VE_GLOBAL/diff_ALL_IDPs_Variance_Explained.png \
        -gravity center -append figs/tmp_2.png
convert figs/tmp_2.png -shave 2x2 -bordercolor black -border 2 figs/Figs/Fig_07.png
echo "Fig 7 created - `date`"

# Create Fig 8
convert figs/MNHT/P_deconf.png figs/MNHT/P_deconf_simple.png \
        -append -shave 1x1 -bordercolor black -border 1 figs/Figs/Fig_08.png
echo "Fig 8 created - `date`"

# Create Fig 9
convert figs/MNHT/BA_ALL_vs_SIMPLE_non-IDP.png -crop 870x690+35+0 -shave 2x2 \
        -bordercolor black -border 2 figs/MNHT/c_BA_ALL_vs_SIMPLE_non-IDP.png
convert figs/MNHT/BA_ALL_vs_NONE_non-IDP.png -crop 870x690+35+0 -shave 2x2 \
        -bordercolor black -border 2 figs/MNHT/c_BA_ALL_vs_NONE_non-IDP.png

convert figs/MNHT/c_BA_ALL_vs_SIMPLE_non-IDP.png \
        figs/MNHT/c_BA_ALL_vs_NONE_non-IDP.png \
        -append figs/Figs/Fig_09.png
echo "Fig 9 created - `date`"

# Create Fig 10
for elem in figs/TC/* ; do
    dire=`dirname $elem`;
    nam=`basename $elem`;
    convert $elem -shave 2x2 -bordercolor black -border 2 $dire/c_$nam;
done
convert -pointsize 50  -annotate +10+645 "A" \
        figs/TC/c_ACQT_Site_1__01.png figs/TC/c_ACQT_Site_1__01.png
convert -pointsize 50  -annotate +10+645 "B" \
        figs/TC/c_correlations.png figs/TC/c_correlations.png
convert -pointsize 50  -annotate +10+645 "C" \
        figs/TC/c_smoothed_IDP.png figs/TC/c_smoothed_IDP.png
convert -pointsize 50  -annotate +10+645 "D" \
        figs/TC/c_unsmoothed_IDP.png figs/TC/c_unsmoothed_IDP.png
convert figs/TC/c_ACQT_Site_1__01.png \
        figs/TC/c_smoothed_IDP.png \
        -append figs/tmp1.png
convert figs/TC/c_correlations.png \
        figs/TC/c_unsmoothed_IDP.png \
        -append figs/tmp2.png
convert figs/tmp1.png figs/tmp2.png  +append figs/Figs/Fig_10.png
echo "Fig 10 created - `date`"


# Create figures for the relationship between eddy (x) and table position (Z)
convert figs/EDDYQC/Eddy_QC_X__vs__TablePos_COG_Z.png \
        -shave 2x2 -bordercolor black -border 2 figs/SM/EDDY_X_-_TABLE_Z.png
echo "Fig SM - EDDY_X / TABLE_POS_Z created - `date`"

# Cropping and adding a border on all temporal component figures
for elem in figs/ACQ*_PCA/* ; do
    dire=`dirname $elem`;
    nam=`basename $elem`;
    convert $elem -crop 692x585+40+0 -shave 2x2 \
            -bordercolor black -border 2 $dire/c_$nam;
done

# Create figures for the Date components
convert figs/ACQD_PCA/c_DATE_Site_1__01.png figs/ACQD_PCA/c_DATE_Site_1__04.png \
        figs/ACQD_PCA/c_DATE_Site_1__07.png figs/ACQD_PCA/c_DATE_Site_1__10.png \
        -append figs/tmp_1.png
convert figs/ACQD_PCA/c_DATE_Site_1__02.png figs/ACQD_PCA/c_DATE_Site_1__05.png \
        figs/ACQD_PCA/c_DATE_Site_1__08.png figs/ACQD_PCA/c_DATE_Site_1__11.png \
        -append figs/tmp_2.png
convert figs/ACQD_PCA/c_DATE_Site_1__03.png figs/ACQD_PCA/c_DATE_Site_1__06.png \
        figs/ACQD_PCA/c_DATE_Site_1__09.png figs/ACQD_PCA/c_DATE_Site_1__12.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQD_S1_1.png

convert figs/ACQD_PCA/c_DATE_Site_1__13.png figs/ACQD_PCA/c_DATE_Site_1__16.png \
        figs/ACQD_PCA/c_DATE_Site_1__19.png figs/ACQD_PCA/c_DATE_Site_1__22.png \
        -append figs/tmp_1.png
convert figs/ACQD_PCA/c_DATE_Site_1__14.png figs/ACQD_PCA/c_DATE_Site_1__17.png \
        figs/ACQD_PCA/c_DATE_Site_1__20.png figs/ACQD_PCA/c_DATE_Site_1__23.png \
        -append figs/tmp_2.png
convert figs/ACQD_PCA/c_DATE_Site_1__15.png figs/ACQD_PCA/c_DATE_Site_1__18.png \
        figs/ACQD_PCA/c_DATE_Site_1__21.png figs/ACQD_PCA/c_DATE_Site_1__24.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQD_S1_2.png

convert figs/ACQD_PCA/c_DATE_Site_1__25.png figs/ACQD_PCA/c_DATE_Site_1__28.png \
        figs/ACQD_PCA/c_DATE_Site_1__31.png \
        -append figs/tmp_1.png
convert figs/ACQD_PCA/c_DATE_Site_1__26.png figs/ACQD_PCA/c_DATE_Site_1__29.png \
        figs/ACQD_PCA/c_DATE_Site_1__32.png \
        -append figs/tmp_2.png
convert figs/ACQD_PCA/c_DATE_Site_1__27.png figs/ACQD_PCA/c_DATE_Site_1__30.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQD_S1_3.png

convert figs/ACQD_PCA/c_DATE_Site_2__01.png figs/ACQD_PCA/c_DATE_Site_2__04.png \
        figs/ACQD_PCA/c_DATE_Site_2__07.png figs/ACQD_PCA/c_DATE_Site_2__10.png \
        -append figs/tmp_1.png
convert figs/ACQD_PCA/c_DATE_Site_2__02.png figs/ACQD_PCA/c_DATE_Site_2__05.png \
        figs/ACQD_PCA/c_DATE_Site_2__08.png figs/ACQD_PCA/c_DATE_Site_2__11.png \
        -append figs/tmp_2.png
convert figs/ACQD_PCA/c_DATE_Site_2__03.png figs/ACQD_PCA/c_DATE_Site_2__06.png \
        figs/ACQD_PCA/c_DATE_Site_2__09.png figs/ACQD_PCA/c_DATE_Site_2__12.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQD_S2_1.png

convert figs/ACQD_PCA/c_DATE_Site_2__13.png figs/ACQD_PCA/c_DATE_Site_2__14.png \
        figs/ACQD_PCA/c_DATE_Site_2__15.png +append figs/SM/ACQD_S2_2.png

convert figs/ACQD_PCA/c_DATE_Site_3__01.png figs/ACQD_PCA/c_DATE_Site_3__04.png \
        figs/ACQD_PCA/c_DATE_Site_3__07.png -append figs/tmp_1.png
convert figs/ACQD_PCA/c_DATE_Site_3__02.png figs/ACQD_PCA/c_DATE_Site_3__05.png \
        figs/ACQD_PCA/c_DATE_Site_3__08.png   -append figs/tmp_2.png
convert figs/ACQD_PCA/c_DATE_Site_3__03.png figs/ACQD_PCA/c_DATE_Site_3__06.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQD_S3_1.png
echo "Date component figures created - `date`"

# Create figures for the Time components
convert figs/ACQT_PCA/c_ACQT_Site_1__01.png figs/ACQT_PCA/c_ACQT_Site_1__04.png \
        figs/ACQT_PCA/c_ACQT_Site_1__07.png figs/ACQT_PCA/c_ACQT_Site_1__10.png \
        -append figs/tmp_1.png
convert figs/ACQT_PCA/c_ACQT_Site_1__02.png figs/ACQT_PCA/c_ACQT_Site_1__05.png \
        figs/ACQT_PCA/c_ACQT_Site_1__08.png figs/ACQT_PCA/c_ACQT_Site_1__11.png \
        -append figs/tmp_2.png
convert figs/ACQT_PCA/c_ACQT_Site_1__03.png figs/ACQT_PCA/c_ACQT_Site_1__06.png \
        figs/ACQT_PCA/c_ACQT_Site_1__09.png figs/ACQT_PCA/c_ACQT_Site_1__12.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQT_S1_1.png

convert figs/ACQT_PCA/c_ACQT_Site_1__13.png figs/ACQT_PCA/c_ACQT_Site_1__16.png \
        -append figs/tmp_1.png
convert figs/ACQT_PCA/c_ACQT_Site_1__14.png figs/ACQT_PCA/c_ACQT_Site_1__17.png \
        -append figs/tmp_2.png
convert figs/ACQT_PCA/c_ACQT_Site_1__15.png figs/ACQT_PCA/c_ACQT_Site_1__18.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQT_S1_2.png

convert figs/ACQT_PCA/c_ACQT_Site_2__01.png figs/ACQT_PCA/c_ACQT_Site_2__04.png \
        figs/ACQT_PCA/c_ACQT_Site_2__07.png figs/ACQT_PCA/c_ACQT_Site_2__10.png \
        -append figs/tmp_1.png
convert figs/ACQT_PCA/c_ACQT_Site_2__02.png figs/ACQT_PCA/c_ACQT_Site_2__05.png \
        figs/ACQT_PCA/c_ACQT_Site_2__08.png figs/ACQT_PCA/c_ACQT_Site_2__11.png \
        -append figs/tmp_2.png
convert figs/ACQT_PCA/c_ACQT_Site_2__03.png figs/ACQT_PCA/c_ACQT_Site_2__06.png \
        figs/ACQT_PCA/c_ACQT_Site_2__09.png figs/ACQT_PCA/c_ACQT_Site_2__12.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQT_S2_1.png

convert figs/ACQT_PCA/c_ACQT_Site_2__13.png figs/ACQT_PCA/c_ACQT_Site_2__16.png \
        figs/ACQT_PCA/c_ACQT_Site_2__19.png -append figs/tmp_1.png
convert figs/ACQT_PCA/c_ACQT_Site_2__14.png figs/ACQT_PCA/c_ACQT_Site_2__17.png \
        figs/ACQT_PCA/c_ACQT_Site_2__20.png -append figs/tmp_2.png
convert figs/ACQT_PCA/c_ACQT_Site_2__15.png figs/ACQT_PCA/c_ACQT_Site_2__18.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQT_S2_2.png

convert figs/ACQT_PCA/c_ACQT_Site_3__01.png figs/ACQT_PCA/c_ACQT_Site_3__04.png \
        figs/ACQT_PCA/c_ACQT_Site_3__07.png figs/ACQT_PCA/c_ACQT_Site_3__10.png \
        -append figs/tmp_1.png
convert figs/ACQT_PCA/c_ACQT_Site_3__02.png figs/ACQT_PCA/c_ACQT_Site_3__05.png \
        figs/ACQT_PCA/c_ACQT_Site_3__08.png figs/ACQT_PCA/c_ACQT_Site_3__11.png \
        -append figs/tmp_2.png
convert figs/ACQT_PCA/c_ACQT_Site_3__03.png figs/ACQT_PCA/c_ACQT_Site_3__06.png \
        figs/ACQT_PCA/c_ACQT_Site_3__09.png figs/ACQT_PCA/c_ACQT_Site_3__12.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQT_S3_1.png

convert figs/ACQT_PCA/c_ACQT_Site_3__13.png figs/ACQT_PCA/c_ACQT_Site_3__16.png \
        figs/ACQT_PCA/c_ACQT_Site_3__19.png -append figs/tmp_1.png
convert figs/ACQT_PCA/c_ACQT_Site_3__14.png figs/ACQT_PCA/c_ACQT_Site_3__17.png \
        figs/ACQT_PCA/c_ACQT_Site_3__20.png -append figs/tmp_2.png
convert figs/ACQT_PCA/c_ACQT_Site_3__15.png figs/ACQT_PCA/c_ACQT_Site_3__18.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/ACQT_S3_2.png
echo "Time component figures created - `date`"

# Create figures for the BA plots for Suppl Material
convert figs/BA_BODY/c_BA_ACQ_DATE_BODY.png \
        figs/BA_COGN/c_BA_ACQ_DATE_COGN.png \
        figs/BA_BODY/c_BA_ACQ_TIME_BODY.png \
        figs/BA_COGN/c_BA_ACQ_TIME_COGN.png \
        -append figs/tmp_1.png
convert figs/BA_BODY/c_BA_AGE_BODY.png \
        figs/BA_COGN/c_BA_AGE_COGN.png \
        figs/BA_BODY/c_BA_SEX_BODY.png \
        figs/BA_COGN/c_BA_SEX_COGN.png \
        -append figs/tmp_2.png
convert figs/BA_BODY/c_BA_AGE_SEX_BODY.png \
        figs/BA_COGN/c_BA_AGE_SEX_COGN.png \
        figs/BA_BODY/c_BA_SITE_BODY.png \
        figs/BA_COGN/c_BA_SITE_COGN.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/BA_1.png

convert figs/BA_BODY/c_BA_DVARS_BODY.png \
        figs/BA_COGN/c_BA_DVARS_COGN.png \
        figs/BA_BODY/c_BA_STRUCT_MOTION_BODY.png \
        figs/BA_COGN/c_BA_STRUCT_MOTION_COGN.png \
        -append figs/tmp_1.png
convert figs/BA_BODY/c_BA_HEAD_MOTION_BODY.png \
        figs/BA_COGN/c_BA_HEAD_MOTION_COGN.png \
        figs/BA_BODY/c_BA_HEAD_MOTION_ST_BODY.png \
        figs/BA_COGN/c_BA_HEAD_MOTION_ST_COGN.png \
        -append figs/tmp_2.png
convert figs/BA_BODY/c_BA_NON_LIN_BODY.png \
        figs/BA_COGN/c_BA_NON_LIN_COGN.png \
        figs/BA_BODY/c_BA_CROSSED_TERMS_BODY.png \
        figs/BA_COGN/c_BA_CROSSED_TERMS_COGN.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/BA_2.png

convert figs/BA_BODY/c_BA_CMRR_BODY.png \
        figs/BA_COGN/c_BA_CMRR_COGN.png \
        figs/BA_BODY/c_BA_PROTOCOL_BODY.png \
        figs/BA_COGN/c_BA_PROTOCOL_COGN.png \
        -append figs/tmp_1.png
convert figs/BA_BODY/c_BA_SCAN_COLD_HEAD_BODY.png \
        figs/BA_COGN/c_BA_SCAN_COLD_HEAD_COGN.png \
        figs/BA_BODY/c_BA_SCAN_HEAD_COIL_BODY.png \
        figs/BA_COGN/c_BA_SCAN_HEAD_COIL_COGN.png \
        -append figs/tmp_2.png
convert figs/BA_BODY/c_BA_SCAN_MISC_BODY.png \
        figs/BA_COGN/c_BA_SCAN_MISC_COGN.png \
        figs/BA_BODY/c_BA_SCAN_RAMP_BODY.png \
        figs/BA_COGN/c_BA_SCAN_RAMP_COGN.png \
        -append figs/tmp_3.png
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/BA_3.png

convert figs/BA_BODY/c_BA_SCALING_BODY.png \
        figs/BA_COGN/c_BA_SCALING_COGN.png \
        figs/BA_BODY/c_BA_TE_BODY.png \
        figs/BA_COGN/c_BA_TE_COGN.png \
        -append figs/tmp_1.png
convert figs/BA_BODY/c_BA_BATCH_BODY.png \
        figs/BA_COGN/c_BA_BATCH_COGN.png \
        figs/BA_BODY/c_BA_NEW_EDDY_BODY.png \
        figs/BA_COGN/c_BA_NEW_EDDY_COGN.png \
        -append figs/tmp_2.png
convert figs/BA_BODY/c_BA_FLIPPED_SWI_BODY.png \
        figs/BA_COGN/c_BA_FLIPPED_SWI_COGN.png \
        figs/BA_BODY/c_BA_FS_T2_BODY.png \
        figs/BA_COGN/c_BA_FS_T2_COGN.png \
        -append figs/tmp_3.png 
convert figs/tmp_1.png figs/tmp_2.png figs/tmp_3.png +append figs/SM/BA_4.png

convert figs/BA_BODY/c_BA_SERVICE_PACK_BODY.png \
        figs/BA_COGN/c_BA_SERVICE_PACK_COGN.png \
        figs/BA_BODY/c_BA_HEAD_SIZE_BODY.png \
        figs/BA_COGN/c_BA_HEAD_SIZE_COGN.png \
        -append figs/tmp_1.png
convert figs/BA_BODY/c_BA_TABLE_POS_BODY.png \
        figs/BA_COGN/c_BA_TABLE_POS_COGN.png \
        figs/BA_BODY/c_BA_EDDY_QC_BODY.png \
        figs/BA_COGN/c_BA_EDDY_QC_COGN.png \
        -append figs/tmp_2.png
convert figs/tmp_1.png figs/tmp_2.png +append figs/SM/BA_5.png
echo "BA plots created - `date`"

# Create Manhattan plots for the Supp Material
convert figs/MNHT/P.png \
        figs/MNHT/P_deconf.png \
        -append figs/SM/MNH_1.png
convert figs/MNHT/P_deconf_simple.png \
        figs/MNHT/P_deconf_PCA99.png \
        -append figs/SM/MNH_2.png
convert figs/MNHT/P_deconf_PCA.png \
        figs/MNHT/P_deconf_PCA90.png \
        -append figs/SM/MNH_3.png
echo "Manhattan plots created - `date`"

# Create violin plots
convert figs/VE_NORMAL/ALL_IDPs_Unique_Variance_Explained.png \
        figs/VE_NORMAL/ALL_IDPs_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_ALL.png
convert figs/VE_NORMAL/T1_Unique_Variance_Explained.png \
        figs/VE_NORMAL/T1_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_T1.png
convert figs/VE_NORMAL/T2_FLAIR_Unique_Variance_Explained.png \
        figs/VE_NORMAL/T2_FLAIR_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_T2_FLAIR.png
convert figs/VE_NORMAL/SWI_Unique_Variance_Explained.png \
        figs/VE_NORMAL/SWI_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_SWI.png
convert figs/VE_NORMAL/FS_Unique_Variance_Explained.png \
        figs/VE_NORMAL/FS_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_FS.png
convert figs/VE_NORMAL/dMRI_Unique_Variance_Explained.png \
        figs/VE_NORMAL/dMRI_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_dMRI.png
convert figs/VE_NORMAL/rfMRI_AMP_Unique_Variance_Explained.png \
        figs/VE_NORMAL/rfMRI_AMP_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_rfMRI_AMP.png
convert figs/VE_NORMAL/rfMRI_CONN_Unique_Variance_Explained.png \
        figs/VE_NORMAL/rfMRI_CONN_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_rfMRI_CONN.png
convert figs/VE_NORMAL/tfMRI_Unique_Variance_Explained.png \
        figs/VE_NORMAL/tfMRI_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/NORMAL_tfMRI.png
convert figs/VE_REDUCED/ALL_IDPs_Unique_Variance_Explained.png \
        figs/VE_REDUCED/ALL_IDPs_Variance_Explained.png \
        -append -crop 1550x2000+190+0 figs/SM/REDUCED_ALL.png
echo "Violin plots created - `date`"

# Create BA plot for the nIDPs
convert figs/MNHT/BA_ALL_vs_PCA99_non-IDP.png \
        figs/MNHT/BA_SIMPLE_vs_PCA99_non-IDP.png \
        -append -shave 2x2 -bordercolor black -border 2 figs/SM/BA_nIDPs.png
echo "BA plot for the nIDPs created - `date`"

# Create GLOBALs
for elem in "T1" "T2_FLAIR" "FS" "SWI" "dMRI" "rfMRI_AMP" "rfMRI_CONN" "tfMRI" ; do
    convert figs/VE_GLOBAL/${elem}_Variance_Explained.png \
        -crop 1670x2000+210+0 figs/tmp_1.png;
    convert figs/tmp_1.png figs/VE_GLOBAL/diff_${elem}_Variance_Explained.png \
        -gravity center -append figs/tmp_2.png;
    convert figs/tmp_2.png -shave 2x2 -bordercolor black -border 2 \
        figs/SM/GLOBAL_${elem}.png;
done
echo "GLOBAL figures created - `date`"

rm figs/tmp*
rm figs/*/c_*.png
