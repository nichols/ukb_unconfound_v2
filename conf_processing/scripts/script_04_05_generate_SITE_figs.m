%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;
addpath('scripts');
addpath('functions');
addpath(genpath('common_matlab/'));

load('workspaces/ws_01/final_workspace.mat')

my_log('', 'Before load');
dataName = '41k';
dataDir = strcat('../data/', dataName, '_data/');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

clear COGN_vars;
clear BODY_names;
clear BODY_vars;
clear COGN_names;
clear GEN_vars;
clear GEN_names;
clear nconf;
clear conf_FST2;
clear conf_NEWEDDY;
clear conf_SCALING_dMRI;
clear conf_SCALING_rfMRI;
clear conf_SCALING_SWI;
clear conf_SCALING_T1;
clear conf_SCALING_T2;
clear conf_SCALING_tfMRI;
clear conf_TablePos_COG_Table;
clear conf_TablePos_COG_X;
clear conf_TablePos_COG_Y;
clear conf_TablePos_COG_Z;
clear conf_TE_rfMRI;
clear conf_TE_tfMRI;
clear conf_YTRANS;
clear head_size_scaling;
clear ind_site;
clear num_conf_groups;
clear QC_IDP_names;
clear QC_IDPs;
clear sex;
clear site;
clear age;
clear ans;
clear repeated_IDs;

siteDATA = getValues(strcat(dataDir, 'ID_SITE.txt'), subject_IDs);
siteValues = my_unique(siteDATA);
indSite=cell(length(siteValues),1);
for j = 1:length(siteValues)
    valueSite=siteValues(j);
    indSite{j}=find(all(siteDATA==siteValues(j,:),2));
end
clear valueSite j;

namesSite = {};
confSite = zeros(size(IDPs_i,1),length(indSite)-1);

% Subjects from Site 1 will have -1 in all site confounds
confSite(indSite{1},:) = -1;

% Subjects for the other sites will have -1 in their corresponding column
% Value by default is 0.
for i=2:length(indSite)
    confSite(indSite{i},i-1) = 1;
    namesSite=[namesSite ; strcat('Site_1_vs_', num2str(i))];
end
confSite = nets_normalise(confSite); 
confSite(isnan(confSite)) = 0;

[confSex,             namesSex]              = getCategorical('SEX',          subject_IDs, dataName);
[confBatch,           namesBatch]            = getCategorical('BATCH',        subject_IDs, dataName);
[confCMRR,            namesCMRR]             = getCategorical('CMRR',         subject_IDs, dataName);
[confProtocol,        namesProtocol]         = getCategorical('PROTOCOL',     subject_IDs, dataName);
[confServicePack,     namesServicePack]      = getCategorical('SERVICEPACK',  subject_IDs, dataName);
[confScanRamp,        namesScanRamp]         = getCategorical('SCANRAMP',     subject_IDs, dataName);
[confScanMisc,        namesScanMisc]         = getCategorical('SCANMISC',     subject_IDs, dataName);
[confScanColdHead,    namesScanColdHead]     = getCategorical('SCANCOLDHEAD', subject_IDs, dataName);
[confScanHeadCoil,    namesScanHeadCoil]     = getCategorical('SCANHEADCOIL', subject_IDs, dataName);
[confFlippedSWI,      namesFlippedSWI]       = getCategorical('FLIPPEDSWI',   subject_IDs, dataName);
[confFST2,            namesFST2]             = getCategorical('FST2',         subject_IDs, dataName);
[confNewEddy,         namesNewEddy]          = getCategorical('NEWEDDY',      subject_IDs, dataName);
[confScaling,         namesScaling]          = getCategorical('SCALING',      subject_IDs, dataName);

[confHeadMotion,      namesHeadMotion]       = getData('HEADMOTION',       subject_IDs, dataName);
[confHeadMotionST,    namesHeadMotionST]     = getData('HEADMOTIONST',     subject_IDs, dataName);
[confHeadSize,        namesHeadSize]         = getData('HEADSIZE',         subject_IDs, dataName);
[confTablePos,        namesTablePos]         = getData('TABLEPOS',         subject_IDs, dataName);
[confDVARS,           namesDVARS]            = getData('DVARS',            subject_IDs, dataName);
[confEddyQC,          namesEddyQC]           = getData('EDDYQC',           subject_IDs, dataName);
[confStructHeadMotion,namesStructHeadMotion] = getData('STRUCTHEADMOTION', subject_IDs, dataName);
[confAge,             namesAge]              = getData('AGE',              subject_IDs, dataName);
[confTE,              namesTE]               = getData('TE',               subject_IDs, dataName);

confAgeSex = confAge .* confSex;
namesAgeSex = {'AgeSex'};

conf       = [confAge confSex confAgeSex confHeadSize confSite  ...
              confCMRR  confServicePack  ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC];

conf_no_site = [confAge confSex confAgeSex confHeadSize  ...
              confCMRR confServicePack  ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC];  
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;;...
         namesCMRR;namesServicePack;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC];          
          
my_log(fm, 'After second load');

ve  = [];
veu = [];
veLowerThreshold = 1e-08;

if check_GPU()
   conf         = gpuArray(conf);
   conf_no_site = gpuArray(conf_no_site);
   IDPs_i       = gpuArray(IDPs_i);
end

conf = nets_demean(conf);
conf(isnan(conf)) = 0;
conf_no_site = nets_demean(conf_no_site);
conf_no_site(isnan(conf_no_site)) = 0;


for i=1:size(IDPs_i,2)
    
    if mod(i,10) == 0
        my_log('', ['Loop: ' num2str(i) ' of ' num2str(size(IDPs_i,2))]);
    end
    
    j=~isnan(IDPs_i(:, i));
    
    confstmp_all = nets_demean(conf(j,:));
    BETAs_all    = (pinv(confstmp_all)*IDPs_i(j, i));
    tmp_all      = confstmp_all * BETAs_all;
    ve_all       = 100 * ((nanstd(tmp_all) ./ nanstd(IDPs_i(j, i))).^2);


    conftmp    = nets_demean(confSite(j,:));
    BETAs      = pinv(conftmp) * IDPs_i(j,i);
    tmp        = conftmp * BETAs;
    ve(i)      = gather(100 * ((nanstd(tmp) ./ nanstd(IDPs_i(j,i))).^2));


    conftmp_exclu = nets_demean(conf_no_site(j,:));
    BETAs_exclu   = pinv(conftmp_exclu) * IDPs_i(j,i);
    tmp_exclu     = conftmp_exclu*BETAs_exclu;
    ve_exclu      = 100 * ((nanstd(tmp_exclu) ./ nanstd(IDPs_i(j,i))).^2);  

    veu(i) = gather(ve_all - ve_exclu);
end

ve( find(ve  < veLowerThreshold)) = veLowerThreshold;
veu(find(veu < veLowerThreshold)) = veLowerThreshold;

if check_GPU()
   ve           = gather(ve);
   veu          = gather(veu);
   conf         = gather(conf);
   conf_no_site = gather(conf_no_site);
   IDPs_i       = gather(IDPs_i);
end

finalData = [log10(ve)' log10(veu)'];

my_log(fm, 'After processing');

dir_figs = 'figs/SITE/';
dir_html = 'HTML/SITE/';
dir_txt  = 'TXT/SITE/';

make_dir(dir_figs);
make_dir(dir_html);
make_dir(dir_txt);

nameOut =strcat(dir_txt, 'DATA.txt');
dlmwrite(nameOut, finalData, 'delimiter', ' ');

IDPNamesFilename = [dir_txt 'names_ALL_IDPs.txt'];
fileID=fopen(IDPNamesFilename,'w');
for k=1:length(IDP_names)
    fprintf(fileID,'%s\n',IDP_names{k});
end
fclose(fileID)

f = figure('visible', 'off', 'PaperOrientation','landscape',...
          'Units','points','Position', [0,0,500,600]);

violinplot(finalData, {'% VE', '% UVE'},  'BoxWidth', 0.02, 'BoxColor', ...
           [0.5 0.5 0.5],  'EdgeColor', [0.2 0.2 0.2]);
      
ti = title('% VE and % UVE of all IDPs for SITE');

set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'FontSize',17, 'yscale', 'linear');
set(gca, 'XGrid', 'off');
set(gca, 'YGrid', 'on');
xtickangle(90);
ylabel('Log_1_0 % of variance explained');

set(gca, 'LooseInset', get(gca, 'TightInset'));
print('-dpng','-r70',strcat('figs/SITE/VE_UVE_SITE.png'));   

htmlFilename = ['HTML/SITE/VE_UVE_SITE.html'] ;

[s, c] = system(['scripts/script_04_05_generate_SITE_figs.py ' nameOut ...
                 ' '  IDPNamesFilename ' ' htmlFilename]);

my_log(fm, 'After image generation');

save('workspaces/ws_04/VEU_SITE.mat', '-v7.3');

my_log(fm, 'End');
