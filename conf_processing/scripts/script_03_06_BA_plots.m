%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
if   exist('workspaces/ws_03/correlations.mat', 'file') == 2
    delete('workspaces/ws_03/correlations.mat');
end
load('workspaces/ws_02/ve_04.mat');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

ALL_IDPs_i_deconf_full=zeros(N,num_IDP);

for i=1:num_IDP
    if mod(i,50) == 0
        my_log(fm, ['Loop: ' num2str(i)]);
    end
    namF = strcat('workspaces/ws_03/IDP_deconf_04/ws_IDP_deconf_', ...
         num2str(i), '.mat');
    % If the generation of the file in the 
    % previous step failed, generate it again 
    if exist(namF, 'file') ~=2
        my_log(fm, string(strcat('File', {' '}, namF, {' '}, ...
                        'does not exist. Regenerating.')));
        func_03_04_BA_plots(i);
    end   
    
    load(namF);
    ALL_IDPs_i_deconf_full(:,i)=IDP_deconf;
end
my_log(fm, 'After loading deconf IDPs. Before loading deconf BODY vars');

%BODY_vars_i = nets_inormal(BODY_vars);
num_BODY_vars=size(BODY_vars,2);
BODY_vars_i_deconf_full=zeros(N,num_BODY_vars);

for i=1:num_BODY_vars
    if mod(i,50) == 0
        my_log(fm, ['Loop: ' num2str(i)]);
    end
    namF = strcat('workspaces/ws_03/BODY_vars_i_deconf_05/ws_BODY_vars_i_deconf_',...
         num2str(i), '.mat');
    % If the generation of the file in the 
    % previous step failed, generate it again 
    if exist(namF, 'file') ~=2
        my_log(fm, string(strcat('File', {' '}, namF, {' '}, ...
                         'does not exist. Regenerating.')));
        func_03_05_BA_plots_BODY(i);
    end   
    
    load(namF);
    BODY_vars_i_deconf_full(:,i)=BODY_vars_i_deconf;
end
my_log(fm, 'After loading deconf BODY_vars');


%COGN_vars_i = nets_inormal(COGN_vars);
num_COGN_vars=size(COGN_vars,2);
COGN_vars_i_deconf_full=zeros(N,num_COGN_vars);

for i=1:num_COGN_vars
    if mod(i,50) == 0
        my_log(fm, ['Loop: ' num2str(i)]);
    end
    namF = strcat('workspaces/ws_03/COGN_vars_i_deconf_05/ws_COGN_vars_i_deconf_',...
         num2str(i), '.mat');
    % If the generation of the file in the 
    % previous step failed, generate it again 
    if exist(namF, 'file') ~=2
        my_log(fm, string(strcat('File', {' '}, namF, {' '}, ...
                         'does not exist. Regenerating.')));
        func_03_05_BA_plots_COGN(i);
    end   
    
    load(namF);
    COGN_vars_i_deconf_full(:,i)=COGN_vars_i_deconf;
end
my_log(fm, 'After loading deconf COGN_vars');


[R_COGN_B, P_COGN_B, N_for_COGN] = my_corr(ALL_IDPs_i_deconf_full,...
                                                  COGN_vars_i_deconf_full);
R_COGN_B   = R_COGN_B(:);
P_COGN_B   = P_COGN_B(:);
N_for_COGN = N_for_COGN(:);

[R_BODY_B, P_BODY_B, N_for_BODY] = my_corr(ALL_IDPs_i_deconf_full,...
                                                  BODY_vars_i_deconf_full);
R_BODY_B   = R_BODY_B(:);
P_BODY_B   = P_BODY_B(:);
N_for_BODY = N_for_BODY(:);

clear i;
clear j;
clear nans_COGN;
clear nans_BODY;
clear nans_IDPs;
clear alt_conf_Group;
clear conf_all_Rand;
clear conf_all_Rand_Group;
clear confRand;
clear confRand_Group;
clear ALL_IDPs;
clear ALL_IDPs_i_deconf_full;
clear confDumb;
clear confOmega013;
clear confOmega072;
clear confOmega182;
clear COGN_unicorrPi_deconf;
clear BODY_unicorrPi_deconf;

rm_make_dir('figs/BA_BODY/');
rm_make_dir('figs/BA_COGN/');
rm_make_dir('TXT_DATA/BA/');
rm_make_dir('workspaces/ws_03/BA_09/');
make_dir('HTML/BA/');

save('workspaces/ws_03/correlations.mat','-v7.3');
my_log(fm, 'End');
