%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
addpath(genpath('common_matlab/'));
addpath('functions/');
            
f1_short = 'jobs/jobs_01_12_gen_ct_conf_short.txt';
f1_long =  'jobs/jobs_01_12_gen_ct_conf_long.txt';

rm_file(f1_short);
rm_file(f1_long);
make_dir('jobs');
make_dir('workspaces/ws_01/veu_ct/');

load('workspaces/ws_01/ct_confounds_generated.mat');

num_IDPs = length(IDPnames);
num1     = floor(num_IDPs * 0.5); % Splitting variables to use 2 SGE queues

% For each IDP
for i = 1:num1
    fileID = fopen([f1_short], 'a');
    fprintf(fileID, './scripts/script_01_12_gen_ct_conf.sh %s \n', num2str(i));
    fclose(fileID);    
end

% For each IDP
for i = num1+1:num_IDPs
    fileID = fopen([f1_long], 'a');
    fprintf(fileID, './scripts/script_01_12_gen_ct_conf.sh %s \n', num2str(i));
    fclose(fileID);    
end

clear;
