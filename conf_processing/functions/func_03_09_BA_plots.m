function func_03_09_BA_plots(confound_group)
%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
    addpath(genpath('common_matlab/'));
    load('workspaces/ws_03/correlations.mat');

    num_groups=num_groups(1);
    group_Names=group_Names{1};
    
    load('workspaces/ws_03/ws_03_BODY_vars_i.mat');
    load('workspaces/ws_03/ws_03_COGN_vars_i.mat');

    label = strrep(group_Names{confound_group}, '_', '');
    
    %Deconfounding IDPs with every confound model
    ind_exclu = ind_conf_groups{1}([1:confound_group-1, ...
                                 confound_group+1:num_groups]);
    ind_exclu = horzcat(ind_exclu{:});
    confs_exclu = conf(:,ind_exclu);

    ALL_IDPs_i_deconf_exclu  = nets_unconfound2(subset_IDPs_i,confs_exclu);
    vars_i_deconf_COGN_exclu = nets_unconfound2(COGN_vars_i,confs_exclu);
    vars_i_deconf_BODY_exclu = nets_unconfound2(BODY_vars_i,confs_exclu);

    [R_COGN_A, P_COGN_A] = my_corr(ALL_IDPs_i_deconf_exclu, ...
                                   vars_i_deconf_COGN_exclu );
    sizes_P_COGN_A = size(P_COGN_A); 
    R_COGN_A = R_COGN_A(:);
    P_COGN_A = P_COGN_A(:);

    [R_BODY_A, P_BODY_A] = my_corr(ALL_IDPs_i_deconf_exclu, ...
                                   vars_i_deconf_BODY_exclu);
    sizes_P_BODY_A = size(P_BODY_A); 
    R_BODY_A = R_BODY_A(:);
    P_BODY_A = P_BODY_A(:);

    my_BA_plot(P_COGN_B, R_COGN_B, P_COGN_A, R_COGN_A, N_for_COGN, ...
               IDPnames, COGN_names, group_Names{confound_group}, ...
               'IDP', 'COGN', sizes_P_COGN_A, 'COGN', 'figs/BA_COGN/', ...
               'TXT_DATA/BA/', 'HTML/BA/',...
               ' After removing all confounds from IDPs and COGN. vars.',...
               [' After removing all confounds but ' ...
                strrep(group_Names{confound_group}, '_', ' ')],...
               '-log_1_0 P values for correlation between COGNITIVE variables and IDPs');

    my_BA_plot(P_BODY_B, R_BODY_B, P_BODY_A, R_BODY_A, N_for_BODY, ...
               IDPnames, BODY_names, group_Names{confound_group}, ...
               'IDP', 'BODY', sizes_P_BODY_A, 'BODY', 'figs/BA_BODY/', ...
               'TXT_DATA/BA/', 'HTML/BA/',...
               ' After removing all confounds from IDPs and BODY. vars.',...
               [' After removing all confounds but ' ...
                strrep(group_Names{confound_group}, '_', ' ')],...
               '-log_1_0 P values for correlation between BODY variables and IDPs');
                      
end
