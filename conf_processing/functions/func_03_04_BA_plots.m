function func_03_04_BA_plots_03(num_IDP)
%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
    addpath(genpath('common_matlab/'));

    load('workspaces/ws_03/ws_03_subset_IDPs_i.mat');
    load('workspaces/ws_03/ws_03_conf.mat');

    IDP_deconf = nets_unconfound2(subset_IDPs_i(:,num_IDP), conf);

    save(strcat('workspaces/ws_03/IDP_deconf_04/ws_IDP_deconf_', ...
         num2str(num_IDP)), '-v7.3', 'IDP_deconf');
end
